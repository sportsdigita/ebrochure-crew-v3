# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.29-log)
# Database: crew-v2-2013
# Generation Time: 2013-12-03 21:35:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table AbstractCustomer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `AbstractCustomer`;

CREATE TABLE `AbstractCustomer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_rep_id` int(11) DEFAULT NULL,
  `first_tab_id` int(11) DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `segment` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `account_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` longtext COLLATE utf8_unicode_ci,
  `state` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_title` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_content` longtext COLLATE utf8_unicode_ci,
  `views` int(11) NOT NULL,
  `last_visited` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `page_title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_picture_title` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_picture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `view_notify` tinyint(1) DEFAULT NULL,
  `invoice_notify` tinyint(1) DEFAULT NULL,
  `organization` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sale_made` tinyint(1) DEFAULT NULL,
  `discr` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `IDX_B98F10CF76311BE2` (`sales_rep_id`),
  KEY `IDX_B98F10CF6DF8393B` (`first_tab_id`),
  CONSTRAINT `FK_B98F10CF6DF8393B` FOREIGN KEY (`first_tab_id`) REFERENCES `tab` (`id`),
  CONSTRAINT `FK_B98F10CF76311BE2` FOREIGN KEY (`sales_rep_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `AbstractCustomer` WRITE;
/*!40000 ALTER TABLE `AbstractCustomer` DISABLE KEYS */;

INSERT INTO `AbstractCustomer` (`id`, `sales_rep_id`, `first_tab_id`, `slug`, `segment`, `account_id`, `address_1`, `address_2`, `city`, `state`, `zip`, `email`, `phone`, `custom_title`, `custom_content`, `views`, `last_visited`, `created`, `page_title`, `custom_picture_title`, `custom_picture`, `view_notify`, `invoice_notify`, `organization`, `sale_made`, `discr`)
VALUES
	(1,2,NULL,'example','renew',NULL,NULL,NULL,NULL,NULL,NULL,'test@email.com',NULL,NULL,NULL,2,'2013-08-20 17:09:13','2013-08-20 16:59:00',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(2,2,NULL,'test','renew',NULL,NULL,NULL,NULL,NULL,NULL,'test@email.com',NULL,NULL,NULL,7,'2013-08-29 10:14:53','2013-08-26 17:32:43',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(3,3,NULL,'jlan','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'jaylandro@gmail.com',NULL,'Pepsi Proposal','<p><span style=\"text-decoration: underline; font-size: x-large;\"><strong><span style=\"text-decoration: underline;\">Pepsi Proposal</span></strong></span></p>\r\n<ul>\r\n<li><span style=\"font-size: large;\">Section 102</span></li>\r\n<li><span style=\"font-size: large;\">Seats 1-30</span></li>\r\n<li><span style=\"font-size: large;\">Complimentary Beverage Service</span></li>\r\n</ul>',15,'2013-12-03 14:08:53','2013-10-22 17:02:02',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(4,3,NULL,'jayland','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'jaylandro@gmail.com',NULL,NULL,NULL,0,NULL,'2013-10-22 17:04:23',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(5,2,NULL,'jayyylann','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'j@j.com',NULL,NULL,NULL,0,NULL,'2013-10-22 17:12:32',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(6,2,NULL,'ac','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'aconover@sportsdigita.com',NULL,'Reebok tab','<blockquote style=\"margin-right: 0px;\" dir=\"ltr\">\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">The San Diego Chargers organization approaches team sponsorship as a true partnership between two organizations that share a similar vision for marketing, activation and community service.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">We are committed to creating a fully integrated platform through the alignment of our mutual values, brand attributes and beliefs that will create the foundation for a long-lasting and impactful partnership.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">&nbsp;</p>\r\n<blockquote style=\"margin-right: 0px;\" dir=\"ltr\">\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">The San Diego Chargers organization approaches team sponsorship as a true partnership between two organizations that share a similar vision for marketing, activation and community service.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">We are committed to creating a fully integrated platform through the alignment of our mutual values, brand attributes and beliefs that will create the foundation for a long-lasting and impactful partnership.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">&nbsp;</p>\r\n<blockquote style=\"margin-right: 0px;\" dir=\"ltr\">\r\n<p style=\"margin: 20px 30px 0px 22px; border: 0px currentColor; text-align: left; line-height: 1.1; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">The San Diego Chargers organization approaches team sponsorship as a true partnership between two organizations that share a similar vision for marketing, activation and community service.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; padding: 0px 0px 0px 30px; border: 0px currentColor; text-align: left; line-height: 1.1; font-family: inherit; font-size: 16px; font-style: inherit; font-variant: inherit; font-weight: inherit; vertical-align: baseline; position: relative;\">We are committed to creating a fully integrated platform through the alignment of our mutual values, brand attributes and beliefs that will create the foundation for a long-lasting and impactful partnership.</p>\r\n</blockquote>\r\n</blockquote>\r\n</blockquote>',1,'2013-11-07 14:26:34','2013-11-07 13:42:48',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(7,8,NULL,'con','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'conover@sportsdigita.com',NULL,'Custom Tab Title','<p style=\"margin: 20px 30px 0px 22px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; line-height: 1.1; vertical-align: baseline; position: relative;\">The San Diego Chargers organization approaches team sponsorship as a true partnership between two organizations that share a similar vision for marketing, activation and community service.</p>\r\n<p style=\"margin: 20px 30px 0px 22px; padding: 0px; border: 0px; font-family: inherit; font-style: inherit; font-variant: inherit; line-height: 1.1; vertical-align: baseline; position: relative;\">We are committed to creating a fully integrated platform through the alignment of our mutual values, brand attributes and beliefs that will create the foundation for a long-lasting and impactful partnership.</p>',0,NULL,'2013-11-07 13:44:20',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(8,8,NULL,'acarney','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'acarney@crew.com',NULL,'Wells Fargo Proposal','<p><span style=\"font-size: x-large;\"><em><strong>Total Investment:&nbsp;</strong></em></span></p>\r\n<p style=\"color: #222222; font-family: Arial, Verdana, sans-serif; font-size: 12px;\"><span style=\"font-size: x-large;\"><em><strong>&nbsp;</strong></em></span></p>\r\n<ul class=\"bullet_round_black\" style=\"padding: 0px; color: #222222; font-size: 12px; margin: 0px 0px 8px; line-height: 1.3em; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;\">\r\n<li style=\"margin: 0px; padding: 0px 0px 0px 11px; list-style: none; background-image: url(\'http://seattle.mariners.mlb.com/images/icons/bullet_round_black.gif\'); background-attachment: scroll; background-position: 0.2em 0.45em; background-repeat: no-repeat no-repeat;\"><span style=\"font-size: large;\"><strong>1 year contract - $175,000</strong></span></li>\r\n</ul>\r\n<ul class=\"bullet_round_black\" style=\"padding: 0px; color: #222222; font-size: 12px; margin: 0px 0px 8px; line-height: 1.3em; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;\">\r\n<li style=\"margin: 0px; padding: 0px 0px 0px 11px; list-style: none; background-image: url(\'http://seattle.mariners.mlb.com/images/icons/bullet_round_black.gif\'); background-attachment: scroll; background-position: 0.2em 0.45em; background-repeat: no-repeat no-repeat;\"><span style=\"font-size: large;\"><strong>3 year contract* - $165,000 per year</strong></span></li>\r\n</ul>\r\n<ul class=\"bullet_round_black\" style=\"padding: 0px; color: #222222; font-size: 12px; margin: 0px 0px 8px; line-height: 1.3em; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;\">\r\n<li style=\"margin: 0px; padding: 0px 0px 0px 11px; list-style: none; background-image: url(\'http://seattle.mariners.mlb.com/images/icons/bullet_round_black.gif\'); background-attachment: scroll; background-position: 0.2em 0.45em; background-repeat: no-repeat no-repeat;\"><span style=\"font-size: large;\"><strong>5 year contract* - $155,000 per year</strong></span></li>\r\n</ul>\r\n<p style=\"color: #222222; font-size: 12px; margin: 0px 0px 8px; padding: 0px; line-height: 1.3em; font-family: HelveticaNeue-Light, \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif;\"><span style=\"font-size: large;\"><strong>* 3% increase in 2014 and 2015</strong></span></p>',1,'2013-11-15 10:57:29','2013-11-15 09:34:37',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer'),
	(9,8,NULL,'cbeacom','acquisition',NULL,NULL,NULL,NULL,NULL,NULL,'cbeacom@crew.com',NULL,'Crew Investment','<p><strong><span style=\"font-size: x-large;\">2014 Total Investment:&nbsp;</span></strong></p>\r\n<ul>\r\n<li><span style=\"font-size: large;\">Section 110</span></li>\r\n<li><span style=\"font-size: large;\">Row 1</span></li>\r\n<li><span style=\"font-size: large;\">Seats 1-4</span></li>\r\n<li><span style=\"font-size: large;\">$5,000 per seat</span></li>\r\n</ul>\r\n<p><span style=\"font-size: x-large;\"><strong>$20,000 total investment&nbsp;</strong></span></p>\r\n<p>Thank you,&nbsp;<br /><br />Derek</p>',3,'2013-11-15 11:32:45','2013-11-15 09:39:28',NULL,NULL,NULL,NULL,NULL,NULL,0,'customer');

/*!40000 ALTER TABLE `AbstractCustomer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table abstractcustomer_offer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abstractcustomer_offer`;

CREATE TABLE `abstractcustomer_offer` (
  `abstractcustomer_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  PRIMARY KEY (`abstractcustomer_id`,`offer_id`),
  KEY `IDX_19FED9A86B6F61D7` (`abstractcustomer_id`),
  KEY `IDX_19FED9A853C674EE` (`offer_id`),
  CONSTRAINT `FK_19FED9A853C674EE` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_19FED9A86B6F61D7` FOREIGN KEY (`abstractcustomer_id`) REFERENCES `AbstractCustomer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table abstractcustomer_tab
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abstractcustomer_tab`;

CREATE TABLE `abstractcustomer_tab` (
  `abstractcustomer_id` int(11) NOT NULL,
  `tab_id` int(11) NOT NULL,
  PRIMARY KEY (`abstractcustomer_id`,`tab_id`),
  KEY `IDX_EB03896B6F61D7` (`abstractcustomer_id`),
  KEY `IDX_EB03898D0C9323` (`tab_id`),
  CONSTRAINT `FK_EB03896B6F61D7` FOREIGN KEY (`abstractcustomer_id`) REFERENCES `AbstractCustomer` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_EB03898D0C9323` FOREIGN KEY (`tab_id`) REFERENCES `tab` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `abstractcustomer_tab` WRITE;
/*!40000 ALTER TABLE `abstractcustomer_tab` DISABLE KEYS */;

INSERT INTO `abstractcustomer_tab` (`abstractcustomer_id`, `tab_id`)
VALUES
	(2,1),
	(2,2),
	(2,3),
	(2,4),
	(2,5),
	(2,6),
	(3,7),
	(3,8),
	(3,9),
	(3,10),
	(3,11),
	(3,12),
	(4,7),
	(4,8),
	(4,9),
	(4,10),
	(4,11),
	(4,12),
	(5,7),
	(5,9),
	(5,12),
	(6,7),
	(6,8),
	(6,10),
	(6,12),
	(7,7),
	(7,8),
	(7,9),
	(7,10),
	(7,12),
	(8,7),
	(8,8),
	(8,10),
	(8,11),
	(8,12),
	(9,7),
	(9,8),
	(9,12),
	(9,13);

/*!40000 ALTER TABLE `abstractcustomer_tab` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table acl_classes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_classes`;

CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table acl_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_entries`;

CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`),
  CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table acl_object_identities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_object_identities`;

CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`),
  CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table acl_object_identity_ancestors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_object_identity_ancestors`;

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`),
  CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table acl_security_identities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acl_security_identities`;

CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table business
# ------------------------------------------------------------

DROP TABLE IF EXISTS `business`;

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `decision_maker` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_8D36E38BF396750` FOREIGN KEY (`id`) REFERENCES `AbstractCustomer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hr_id` int(11) NOT NULL,
  `first_name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company_id` int(11) NOT NULL,
  `company` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `purl` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table contact_tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contact_tag`;

CREATE TABLE `contact_tag` (
  `contact_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`,`tag_id`),
  KEY `IDX_FEB3D6BBE7A1254A` (`contact_id`),
  KEY `IDX_FEB3D6BBBAD26311` (`tag_id`),
  CONSTRAINT `FK_FEB3D6BBBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FEB3D6BBE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `start_year` longtext COLLATE utf8_unicode_ci,
  `membership_level` longtext COLLATE utf8_unicode_ci,
  `section` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `row` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `seats` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_seats` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_per_seat` longtext COLLATE utf8_unicode_ci,
  `plan_code` longtext COLLATE utf8_unicode_ci,
  `total` longtext COLLATE utf8_unicode_ci,
  `pc` longtext COLLATE utf8_unicode_ci,
  `final_total` longtext COLLATE utf8_unicode_ci,
  `alt_name` longtext COLLATE utf8_unicode_ci,
  `decision_maker` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_csv` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_81398E09BF396750` FOREIGN KEY (`id`) REFERENCES `AbstractCustomer` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;

INSERT INTO `customer` (`id`, `first_name`, `last_name`, `start_year`, `membership_level`, `section`, `row`, `seats`, `num_seats`, `price_per_seat`, `plan_code`, `total`, `pc`, `final_total`, `alt_name`, `decision_maker`, `logo`, `full_csv`)
VALUES
	(1,'Test','Customization',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,'Johnny','Longlastname',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,'James','Landro',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-Hd-Logos.png',NULL),
	(4,'jayy','landroo',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,'jayyy','lannn',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(6,'Alex','Conover',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Decision Maker','reeboklogo.jpg',NULL),
	(7,'Alex','Conover',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,'Adam','Carney',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Adam Carney','wellsfargov2.jpg',NULL),
	(9,'Clark','Beacom',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Clark Beacom','170px-Columbus_Crew_logo.svg.png',NULL);

/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table email
# ------------------------------------------------------------

DROP TABLE IF EXISTS `email`;

CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(127) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` longtext COLLATE utf8_unicode_ci NOT NULL,
  `tags` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `send` datetime DEFAULT NULL,
  `sent` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table offer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `offer`;

CREATE TABLE `offer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `main_image` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tab_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `alt_text` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `view` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `view_index` (`view`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table renewal
# ------------------------------------------------------------

DROP TABLE IF EXISTS `renewal`;

CREATE TABLE `renewal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `payment` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `playoff` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `addtl_playoff` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_FD0447C89395C3F3` (`customer_id`),
  CONSTRAINT `FK_FD0447C89395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table shared
# ------------------------------------------------------------

DROP TABLE IF EXISTS `shared`;

CREATE TABLE `shared` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_138CF4BB9395C3F3` (`customer_id`),
  CONSTRAINT `FK_138CF4BB9395C3F3` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table tab
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tab`;

CREATE TABLE `tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL,
  `segment` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `standard` tinyint(1) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tab` WRITE;
/*!40000 ALTER TABLE `tab` DISABLE KEYS */;

INSERT INTO `tab` (`id`, `title`, `short_name`, `sort_order`, `segment`, `standard`, `parent`)
VALUES
	(1,'Welcome','welcome',0,'renew',1,NULL),
	(2,'Letter from Anthony Precourt','letter',1,'renew',1,NULL),
	(3,'Message from Mark McCullers','message',2,'renew',1,NULL),
	(4,'Upgrade Your Seat','upgrade',3,'renew',1,NULL),
	(5,'Testimonials','testimonials',4,'renew',1,NULL),
	(6,'Massive Moments','moments',5,'renew',1,NULL),
	(7,'Welcome','welcome',0,'acquisition',1,NULL),
	(8,'Premier Seating','premier',3,'acquisition',1,NULL),
	(9,'Group Tickets','group',4,'acquisition',1,NULL),
	(10,'Corporate Hospitality','hospitality',5,'acquisition',1,NULL),
	(11,'Testimonials','testimonials',6,'acquisition',1,NULL),
	(12,'Custom','custom',8,'acquisition',0,NULL),
	(13,'Season Ticket','seastick',1,'acquisition',1,NULL),
	(14,'Corporate Outing','corpout',2,'acquisition',1,NULL),
	(15,'Group Experiences','grpexp',7,'acquisition',1,NULL);

/*!40000 ALTER TABLE `tab` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tag`;

CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hr_id` int(11) DEFAULT NULL,
  `name` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table u_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `u_group`;

CREATE TABLE `u_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_750629085E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`, `name`, `phone_number`, `slug`, `photo`, `title`, `chat`)
VALUES
	(2,'tombowden','tombowden','tbowden@thecrew.com','tbowden@thecrew.com',1,'exmcatistdcscsoo4wwsogos0osk484','NU+K7Kd+9oJM60HMm3K8GoXwP3h3++Oj2U8z52O7PqiCdTPdWYalxen/BftirlRkv7oTQuozGth8Ia2BQ0I4rQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'2013-08-15 09:47:08','2013-08-15 09:47:08',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Tom Bowden','614-447-4143','tombowden','Tom_Bowden.jpg','Sales Representative',NULL),
	(3,'madelinedavis','madelinedavis','mdavis@thecrew.com','mdavis@thecrew.com',1,'b8ytavcy1hs84wwkw8k4gc0go8o4sws','tUMebIpLCiDHJhmrFYY0XTIt9jtq4bSHCmPJqE8ZcUYix2j1Uvmesvr8TVJPqe0AN9EaA1jq8egjwRI3vJvvFQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'2013-08-15 09:47:54','2013-08-15 09:47:54',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Madeline Davis','614-447-4240','madelinedavis','Madeline_Davis.jpg','Sales Representative',NULL),
	(4,'tylerhill','tylerhill','thill@thecrew.com','thill@thecrew.com',1,'1at2q34qd5wk08400s4ckcckko0c0k8','GAUbBqHT5XwQCqOGILXKNihJz1y2gKGvWrmX47pp56p0Mi/1xGwlS/EZM/6H/HW2RyVg1t195iEY6HMxuzsGRQ==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'2013-08-15 09:48:31','2013-08-15 09:48:31',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Tyler Hill','614-447-4214','tylerhill','Tyler_Hill.jpg','Sales Representative',NULL),
	(5,'jessicatroth','jessicatroth','jtoth@thecrew.com','jtoth@thecrew.com',1,'b8c1w6wjq680cwwsgws0w0w8cko8488','DIg7pDyG9i5/5LH+8kiZWIQ5lVB4eJgoxQu7JAYufimLICNK1lPWB+q47PmzPGDQdmrf9/JYO2KtkO0Myn3UNA==',NULL,0,0,NULL,NULL,NULL,'a:0:{}',0,NULL,'2013-08-15 09:49:07','2013-08-15 09:49:07',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Jessica Toth','614-447-4292','jessicatroth','Jessica_Toth.jpg','Sales Representative',NULL),
	(6,'jay','jay','jlandro@sportsdigita.com','jlandro@sportsdigita.com',1,'r87kachhcz4s44owkkkckwkgo8g0gwc','prZgANnWAk32gEC8ICh/LObIN1bRgqNTkacqjqTJaR7Jppy2J3E96qlz719ItC99qL/kUfaCz49FEyvastak/g==','2013-11-20 10:55:26',0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL,'2013-10-22 16:22:27','2013-11-20 10:55:27',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'',NULL,NULL,NULL,'Sales Representative',NULL),
	(8,'Derek','derek','dgoodnature@sportsdigita.com','dgoodnature@sportsdigita.com',1,'b4amcg81d3wwkccgw08s4ks0cs00sww','zUy7qxolasDZwg+eaCEBgT/nfvJQHJiFvQMXZ+AjSXQQLbNlaCXwdrrjb9lMsI0w1r5nIIH4o8WadAI7gugFuw==','2013-11-26 10:51:59',0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',0,NULL,'2013-10-24 10:27:19','2013-11-26 10:51:59',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Derek Goodnature',NULL,'dgoodnature',NULL,'Sales Representative',NULL),
	(10,'acarney@thecrew.com','acarney@thecrew.com','acarney@thecrew.com','acarney@thecrew.com',1,'kcrsei2pd5w4swkwc0okw4ccoogc48w','ov+X3zsNuNDANpurjurEu61bSRlKiqHarOgZcvEYILBtY6kRsiDEMjNVtrezXIQbkiIQUK7Njym/38nomlTtVg==',NULL,0,0,NULL,NULL,NULL,'a:1:{i:0;s:10:\"ROLE_SALES\";}',0,NULL,'2013-11-26 10:53:12','2013-11-26 10:53:12',NULL,NULL,NULL,NULL,NULL,'u',NULL,NULL,NULL,NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'null',NULL,NULL,'Adam Carney',NULL,'acarney',NULL,'Sales Representative',NULL);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_group`;

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_8F02BF9DA76ED395` (`user_id`),
  KEY `IDX_8F02BF9DFE54D947` (`group_id`),
  CONSTRAINT `FK_8F02BF9DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_8F02BF9DFE54D947` FOREIGN KEY (`group_id`) REFERENCES `u_group` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table view
# ------------------------------------------------------------

DROP TABLE IF EXISTS `view`;

CREATE TABLE `view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `time` datetime NOT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(63) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FEFDAB8EE7A1254A` (`contact_id`),
  CONSTRAINT `FK_FEFDAB8EE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
