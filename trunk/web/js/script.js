/* Author:
sportsdigita 2013
*/

$.getDocHeight = function(){
    return Math.max(
        $(document).height(),
        $(window).height(),
        /* For opera: */
        document.documentElement.clientHeight
    );
};

$.getDocWidth = function(){
    return Math.max(
        $(document).width(),
        $(window).width(),
        /* For opera: */
        document.documentElement.clientWidth
    );
};

var boxCycleDelay = 6000; // Delay between rotating bottom boxes
var shineDelay = 10000; // Delay between "shines" over side tabs
var profileDelay = 8000; // Delay before the profile tab auto-hides after being displayed
var callActionDelay = 2000; // Interval for the call to action animation

var getDots = (function () { // Generates dots for the "Loading..." animation
    var dots = -2;
    return function () {
        var str = "";
        var i;
        dots++;
        if (dots > 3) {
            dots = 0;
        }
        for (i = 0; i < dots; i += 1) {
            str = str + ".";
        }
        return str;
    };
}());

$(document).ready(function(){

    $('#navscrollbtn').click(function(){
        $(".killscroll").animate({ scrollTop: 225 }, 200);
        $('#navscrollbtn').hide();
        $('.nav-item-up').show();
    });
    $('#navscrollbtnup').click(function(){
        $(".killscroll").animate({ scrollTop: 0 }, 200);
        $('.nav-item-up').hide();
        $('#navscrollbtn').show();
    });


    if ($('.personalize').text().length > 28) {
        $('.personalize').css({'font-size': '30px', 'top': '0px'});
    }

    if (window.location.pathname.length < 2) {
        $('#my-profile').hide();
    }

    $('.nav-item').click(function(){
       //TODO
        if(jwplayer('intro')!=undefined){
            jwplayer('intro').pause();
        }

    });


//************ SETUP AUDIO+VIDEO ************
    jwgo();

//************ GO HOME ************

    var goHome = (function() {
        $('#video-contain').fadeOut();
        site.animate({'marginTop': '0px'}, 1000);
        loading.fadeOut(600);
        setTimeout(function(){
            if (window.firstTab) {
                $('.tab-' + window.firstTab).click();
            }
        }, 800);
    });

//************ LOADING SCREEN AND INTRO VIDEO ************

    var loading = $('#loading-contain');
    var site = $('#wrapper');

    // if (!window.location.href.match(/\#/)) {
    //     site.css("marginTop", "-895px");

    //     $('.logo-outline').delay(500).fadeIn(1200);
    //     $('.logo').delay(1800).fadeIn(1800);
    //     $('.loading-bg').delay(1800).fadeOut(1800);
    //     $('.logo-outline').delay(1800).fadeOut(10);

    //     setTimeout(function () {
    //         $('.logo').animate({'width': '300px', 'height': '204px', 'top': '-170px', 'right': '210px'}, 600);
    //         $('#video-contain').delay(100).fadeIn();

    //         setTimeout(function() {
    //             $('#logo-contain').css('z-index', '40');
    //         }, 900);
    //     }, 4200);
    // } else {
        goHome();
    // }



    $('.nav-item').first().addClass('active');

    /* an infinite loop animation for the image slideshows */
    var indexCount = 0;
    var adSwapBoxes = $('#lower-content').find($('.ad-swap')); /* get number of active adswap boxes */
    var adSwapValue = adSwapBoxes.size();
    var imageCycle = function () {
        $('.ad-swap:eq(' + indexCount + ')').each(function (i) {
            var delay = (i + 1) * 2000;
            $(this).children("div:last").delay(delay).animate(
                {'opacity' : '0', 'z-index' : '0'},
                500,
                function () {
                    var $currentImage = $(this);
                    $currentImage.parent().prepend($currentImage);
                    $currentImage.css({'opacity' : '1'});
                }
            );
            indexCount++;
            if (indexCount === adSwapValue) {
                indexCount = 0;
            }
        });
    };

    var popupAction = function () {
        var popupContain = $('.popup-contain');
        var popupItem = $('.popup');
        var hasPop = $('.haspop');
        var popupItems = $('#wrapper').find(popupItem);

        hasPop.each(function () {
            $(this).click(function () {
                // eval("var target = $('." + $(this).data('popup') + "')");
                var target = $('.' + $(this).data('popup'));
                var video = target.children().children().attr('id');
                popupContain.fadeIn();
                popupItem.hide();
                target.show();
                if (video) {
                    jwplayer(video).play();
                }
            });
        });
        $('.close-pop').click(function () {
            jwplayer('mccullers').stop();
            popupContain.fadeOut();
        });
        $('.popup-catch').click(function() {
            jwplayer('mccullers').stop();
            popupContain.fadeOut();
        });
    };
    popupAction();

    var sliderNavInfinite = (function () { /* For infinite rotation, without specific slide navigation */
        /* var slideContain = $('.slides'); */
        var nextArrow = $("#slide-contain .next-arrow");
        var prevArrow = $("#slide-contain .prev-arrow");
        var navItem = $('.nav-item');
        var slideContent = $('#slide-contain .slider').children();
        var slidesClass = $('#slide-contain .slides');
        var slideWidth = $('#slide-contain .slider').width();
        var currentTab = 0;
        var currentSection = $(this).index();
        var suitesButton = $('#slide-contain .suites');

        /* hide all content sections */
        slidesClass.hide();
       /*  only show the first content section initially */
        slidesClass.first().show();

        /* sets width of all slide containers to twice the width of the main container that's set to overflow hidden */
        slidesClass.css({'width' : (slideWidth * 3)});

        var slideItems = $(slidesClass[0]).find($('#slide-contain .slide-item')).size(); /* hide the arrows? */
        if (slideItems === 1) {
            nextArrow.hide();
            prevArrow.hide();
        } else {
            nextArrow.show();
            prevArrow.show();
        }

        /* controls which slide container displays when clicking the navigation items and sets the variable currentTab*/
        navItem.each(function (index) {
            var slidesArray = $(slidesClass[index]).find($('#slide-contain .slide-item'));
            var slideItems = slidesArray.size();
            $(this).data('original_state', slidesArray);
            $(this).click(function () {
                if (slideItems === 1) {
                    nextArrow.hide();
                    prevArrow.hide();
                } else {
                    nextArrow.show();
                    prevArrow.show();
                }
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                slidesClass.hide();
                slideContent.eq(index).html($(this).data('original_state'));
                slideContent.eq(index).show();
                currentTab = $(this).index();

                if (jwplayer('testimonial') != null){
                    jwplayer('testimonial').stop();
                    jwplayer('testimonial').remove();
                }
                if (jwplayer('intro') != null){
                    jwplayer('intro').stop();
                    jwplayer("intro").remove();
                }
                if (jwplayer('ticketcardtutorialvideo') != null){
                    jwplayer('ticketcardtutorialvideo').stop();
                    jwplayer("ticketcardtutorialvideo").remove();
                }

                jwgo();
                popupAction();
            });
        });

        /* next content slide animation */
        var animateNext = function () {
            slidesClass.stop();
            var slideArray = $(slidesClass[currentTab]).find($('#slide-contain .slide-item'));
            if ((slideArray.size()) !== 1) {
                slidesClass.animate({'left' : ((slideWidth) * -1)}, 250, function () {
                    $(slideArray[slideArray.size() - 1]).after($(slideArray[0]));
                    slidesClass.css({'left' : '0px'});
                });
            }
        };

        var fastNext = function () {
            var slideArray = $(slidesClass[currentTab]).find($('#slide-contain .slide-item'));
            if ((slideArray.size()) !== 1) {
                slidesClass.animate({'left' : (slideWidth * -1)}, 0, function () {
                    $(slideArray[slideArray.size() - 1]).after($(slideArray[0]));
                    slidesClass.css({'left' : '0'});
                });
            }
        };

        /* previous content slide animation */
        var animatePrev = function () {
            slidesClass.stop();
            var slideArray = $(slidesClass[currentTab]).find($('#slide-contain .slide-item'));
            if ((slideArray.size()) !== 1) {
                slidesClass.css({'left' : (slideWidth * -1)});
                $(slideArray[0]).before(slideArray[slideArray.size() - 1]);
                slidesClass.animate({'left' : '0'}, 250);
            }
        };

        var autoNext = function () {
            animateNext();
        };

        var thing;
        setTimeout(function () {
            thing = setInterval(autoNext, 15000);
        }, 15000);

        $('#slide-contain .slide-item').click(function () {
            clearInterval(thing);
        });

        nextArrow.click(function () {
            animateNext();
            clearInterval(thing); /* clear the autoNext interval since the user is manually navigating this content */
        });

        prevArrow.click(function () {
            animatePrev();
            clearInterval(thing); /* clear the autoNext interval since the user is manually navigating this content*/
        });

        $(document).on('click', '#slide-contain .suites', function () {
            fastNext();
        });

        $(document).on('click', '#slide-contain .scotts-field', function () {
            for (var i = 0; i < 3; i++) {
                fastNext();
            }
        });

        $(document).on('click', '#slide-contain .center-circle', function () {
            for (var i = 0; i < 5; i++) {
                fastNext();
            }
        });

        $(document).on('click', '#slide-contain .premier-east', function () {
            for (var i = 0; i < 7; i++) {
                fastNext();
            }
        });

        $(document).on('click', '#slide-contain .corp1', function () {
            fastNext();
        });

        $(document).on('click', '#slide-contain .corp2', function () {
            for (var i = 0; i < 3; i++) {
                fastNext();
            }
        });

        $(document).on('click', '#slide-contain .corp3', function () {
            for (var i = 0; i < 4; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .corp4', function () {
            for (var i = 0; i < 6; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .corpout-vil', function () {
            for (var i = 0; i < 1; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .corpout-stad', function () {
            for (var i = 0; i < 2; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .corpout-fiel', function () {
            for (var i = 0; i < 3; i++) {
                fastNext();
            }
        });
        //
        $(document).on('click', '#slide-contain .premier-1', function () {
            for (var i = 0; i < 1; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .premier-2', function () {
            for (var i = 0; i < 2; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .premier-3', function () {
            for (var i = 0; i < 3; i++) {
                fastNext();
            }
        });
        $(document).on('click', '#slide-contain .premier-4', function () {
            for (var i = 0; i < 4; i++) {
                fastNext();
            }
        });

    }());


/******************LOWER SLIDER***********************/

    var sliderNavInfinite = (function () { /* For infinite rotation, without specific slide navigation */
        /* var slideContain = $('.slides'); */
        var nextArrow = $("#slide-contain-lower .next-arrow");
        var prevArrow = $("#slide-contain-lower .prev-arrow");
        var navItem = $('#slide-contain-lower .nav-item');
        var slideContent = $('#slide-contain-lower .slider').children();
        var slidesClass = $('#slide-contain-lower .slides');
        var slideWidth = $('#slide-contain-lower .slider').width();
        var currentTab = 0;
        var currentSection = $(this).index();

        /* hide all content sections */
        slidesClass.hide();
       /*  only show the first content section initially */
        slidesClass.first().show();

        /* sets width of all slide containers to twice the width of the main container that's set to overflow hidden */
        slidesClass.css({'width' : (slideWidth * 4)});

        var slideItems = $(slidesClass[0]).find($('#slide-contain-lower .slide-item')).size(); /* hide the arrows? */
        if (slideItems === 1) {
            nextArrow.hide();
            prevArrow.hide();
        } else {
            nextArrow.show();
            prevArrow.show();
        }

        /* controls which slide container displays when clicking the navigation items and sets the variable currentTab*/
        navItem.each(function (index) {
            var slidesArray = $(slidesClass[index]).find($('#slide-contain-lower .slide-item'));
            var slideItems = slidesArray.size();
            $(this).data('original_state', slidesArray);
            $(this).click(function () {
                if (slideItems === 1) {
                    nextArrow.hide();
                    prevArrow.hide();
                } else {
                    nextArrow.show();
                    prevArrow.show();
                }
                $(this).siblings().removeClass('active');
                $(this).addClass('active');
                slidesClass.hide();
                slideContent.eq(index).html($(this).data('original_state'));
                slideContent.eq(index).show();
                currentTab = $(this).index();
                popupAction();
            });
        });

        /* next content slide animation */
        var animateNext = function () {
            slidesClass.stop();
            var slideArray = $(slidesClass[currentTab]).find($('#slide-contain-lower .slide-item'));
            if ((slideArray.size()) !== 1) {
                slidesClass.animate({'left' : (slideWidth * -1)}, 250, function () {
                    $(slideArray[slideArray.size() - 1]).after($(slideArray[0]));
                    slidesClass.css({'left' : '0'});
                });
            }
        };

        /* previous content slide animation */
        var animatePrev = function () {
            slidesClass.stop();
            var slideArray = $(slidesClass[currentTab]).find($('#slide-contain-lower .slide-item'));
            if ((slideArray.size()) !== 1) {
                slidesClass.css({'left' : (slideWidth * -1)});
                $(slideArray[0]).before(slideArray[slideArray.size() - 1]);
                slidesClass.animate({'left' : '0'}, 250);
            }
        };

        var autoNext = function () {
            animateNext();
        };

        var thing;
        setTimeout(function () {
            thing = setInterval(autoNext, 6000);
        }, 6000);

        $('#slide-contain-lower .slide-item').click(function () {
            clearInterval(thing);
        });

        nextArrow.click(function () {
            animateNext();
            clearInterval(thing); /* clear the autoNext interval since the user is manually navigating this content */
        });
        prevArrow.click(function () {
            animatePrev();
            clearInterval(thing); /* clear the autoNext interval since the user is manually navigating this content*/
        });

    }());


    var sliderNavFixed = (function () {
       var slide = $('.slide-item');
       var slideWidth = $('.slide-item').width();
       var mainNavItem = $('.nav-item');
       sliderWidth = $('#slide-contain').width();

       var slideContent = $('.slider').children();
       var slidesClass = $('.slides');
       var currentTab = 0;
       var sliderNav = $('.slider-nav');

       var nextArrow = $(".next-arrow");
       var prevArrow = $(".prev-arrow");
       prevArrow.hide();
       nextArrow.show();
       /* hide all content sections */
        slidesClass.hide();
       /*  only show the first content section initially */
        slidesClass.first().show();

        var createSubNav = (function () {
           var navArray = jQuery(slidesClass[currentTab]).find($('.slide-item'));
           var navItems = navArray.size();
           NavItemWidth = Math.floor((sliderWidth / navItems));
           sliderNav.empty();
           if (navItems > 2) {
               sliderNav.show();
               nextArrow.hide();
               prevArrow.hide();
           } else if (navItems == 1) {
               sliderNav.hide();
               nextArrow.hide();
               prevArrow.hide();
           } else {
               sliderNav.hide();
               nextArrow.show();
           }
           for(var i = 0; i < navItems; i++) {
                var title = (jQuery(slidesClass[currentTab]).children().eq(i)).attr('subnav');
                sliderNav.append('<div class="slider-nav-item nav'+ i +'" ><p>'+ title +'</p></div>');
                /* style="width: '+ NavItemWidth +'px;" */
           }
           $('.slider-nav-item').eq(0).addClass('active');
           $('.slider-nav-item').each(function () {
                jQuery(this).click(function (index) {
                    jQuery(slidesClass[currentTab]).animate({'left' : (((jQuery(this).index()) * sliderWidth) * -1)});
                    jQuery(this).siblings().removeClass('active');
                    jQuery(this).addClass('active');
               });
           });
        });
        createSubNav();

        slidesClass.each(function(index) {
           var slides = jQuery(this).find($('.slide-item'));
           var slideCount = slides.size();
           jQuery(this).css({'width' : (slideCount * sliderWidth)});
        });

        mainNavItem.each(function(index) { /* basic, main level tab navigation based on index */
            var slidesArray = jQuery(slidesClass[index]).find($('.slide-item'));
            var slideItems = slidesArray.size();

            jQuery(this).data('original_state', slidesArray);
            jQuery(this).click(function () {

                jQuery(this).siblings().removeClass('active');
                jQuery(this).addClass('active');
                slidesClass.hide();
                slidesClass.css({'left' : '0'});
                slideContent.eq(index).show();
                currentTab = jQuery(this).index();
                createSubNav();
                popupAction();
            });
        });

        var animateNext = (function () {
            slidesClass.stop();
            var slideWidth = $('#slide-contain').width();
            var currentSlider = slidesClass.eq(currentTab);
            var currentPosition = currentSlider.position().left;
            currentSlider.animate({'left' : currentPosition - slideWidth});

        });
        var animatePrev = (function () {
            slidesClass.stop();
            var slideWidth = $('#slide-contain').width();
            var currentSlider = slidesClass.eq(currentTab);
            var currentPosition = currentSlider.position().left;
            currentSlider.animate({'left' : currentPosition + slideWidth});

        });
        nextArrow.click(function () {
            animateNext();
            jQuery(this).hide();
            prevArrow.show();

        });
        prevArrow.click(function () {
            animatePrev();
            jQuery(this).hide();
            nextArrow.show();

        });
    });

//************ Countdown Clock ************
    function getTimeUntil(date) {
        var diff = new Date(date).getTime() - new Date().getTime();
        var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000;
        var days = '' + Math.floor (diff / cd);
        var hours = '0' + Math.floor( (diff - days * cd) / ch);
        var minutes = '0' + Math.floor( (diff - days * cd - hours * ch) / 60000);
        var seconds = '0' + Math.floor( (diff - days * cd - hours * ch - minutes * 60000) / 1000);
        return {
            "days": days,
            "hours": hours.slice(-2),
            "minutes": minutes.slice(-2),
            "seconds": seconds.slice(-2)
        };
    }
    var diff = getTimeUntil("October 17, 2014 17:00:00");
    $(".countdown .days").html(diff.days);
    $(".countdown .hours").html(diff.hours);
    $(".countdown .minutes").html(diff.minutes);
    $(".countdown .seconds").html(diff.seconds);

    var seconds;
    setInterval(function() {
        var diff = getTimeUntil("October 17, 2014 17:00:00");
        $(".countdown .days").html(diff.days);
        $(".countdown .hours").html(diff.hours);
        $(".countdown .minutes").html(diff.minutes);
        $(".countdown .seconds").html(diff.seconds);
    }, 10);




    // Removed Acquisition Countdown for now
    /*var diff2 = getTimeUntil("October 26, 2014 17:00:00");
    $(".countdown2 .days").html(diff2.days);
    $(".countdown2 .hours").html(diff2.hours);
    $(".countdown2 .minutes").html(diff2.minutes);
    $(".countdown2 .seconds").html(diff2.seconds);

    var seconds2;
    setInterval(function() {
        var diff2 = getTimeUntil("October 26, 2014 17:00:00");
        $(".countdown2 .days").html(diff2.days);
        $(".countdown2 .hours").html(diff2.hours);
        $(".countdown2 .minutes").html(diff2.minutes);
        $(".countdown2 .seconds").html(diff2.seconds);
    }, 10);*/




    $('#my-profile').unbind("click");

    $('#my-profile').click(function(e) {
        e.stopPropagation();
        var popupContain = $('.popup-contain');
        var popupItem = $('.popup');
        var target = $('.' + $(this).data('popup'));
        popupContain.fadeIn();
        popupItem.hide();
        target.show();
    });

    $('.realign').live("click", function(e) {
        e.stopPropagation();
        console.log('click');
        $('.nav-3').click();
    });

    $('#personalize-jersey-link').click(function () {
        var personalizeJersey = $('#personalize-jersey-contain');
        var stitchLink = $('#stitch-jersey-link');
        var personalizeJerseyLink = $('#personalize-jersey-link');
        if (!personalizeJersey.hasClass('active')) {
            personalizeJerseyLink.css('background', 'url("../images/buttons/pixel.png") no-repeat');
            personalizeJersey.animate({
                width: '100%'
            }, 800, function () {
                personalizeJersey.addClass('active');
                stitchLink.show();
            });
        } else {
            personalizeJerseyLink.css('background', 'url("../images/buttons/PersonalizeYourJersey-nonactive.png") no-repeat');
            personalizeJersey.animate({
                width: '36px'
            }, 800, function () {
                personalizeJersey.removeClass('active');
                stitchLink.hide();
            });
        }
    });

    $('.rep-link').click(function () {

        /*var contactRep = $('.contact-rep');
        var repLink = $('.rep-link');
        if (!contactRep.hasClass('active')) {
            repLink.css('background', 'none');
            $('.rep-photo').css('right', '10px');
            contactRep.animate({
                height: '156px',
                width: '396px'
            }, 400, function () {
                contactRep.addClass('active');
            });
        } else {*/
            /*repLink.css('background', 'url("../images/buttons/contact-rep-button.png") no-repeat');*/
            /*$('.rep-photo').css('right', '410px');
            contactRep.animate({
                height: '35px',
                width: '196px'
            }, 400, function () {
                contactRep.removeClass('active');
            });
        }*/
        //$('.popup-contain').fadeIn();
        //$('.popup-contact').show();
    });

//************* Personalized Jersey ****************
    var personal = (function () {
        var frame = $('.personal-frame');
        var closebg = $('.personal-catch');
        var launch = $('#stitch-jersey-link');
        var shareLaunch = $('#personal-btn-share');
        var img = $("#person-img");
        var name = $("#personal-name");
        var number = $("#personal-number");
        var shareName = $('#personal-name-share');
        var shareNumber = $('#personal-number-share');
        var siteUrl = window.location.href;
        var url = 'http://' + siteUrl.split('/').slice(2,3) + '/';;
        number.focusin(function() {
            number.val('');
        });
        name.focusin(function() {
            name.val('');
        });
        shareNumber.focusin(function() {
            shareNumber.val('');
        });
        shareName.focusin(function() {
            shareName.val('');
        });
        launch.click(function() {
            var personName = name.val();
            var personNumber = number.val();
            if(personNumber == ''){
                number.focus();
            }
            if(personName == ''){
                name.focus();
            }
            else{
                closebg.fadeIn();
                frame.fadeIn();
                $('.personal-close').fadeIn();
                $('.personalized-jersey').animate({'right': '0'}, 100);
                $.ajax({
                  type: "GET",
                  url: "/_jersey/generate",
                  data: {
                        name: personName,
                        number: personNumber
                    },
                success: function(res) {
                    if(res == 'error'){
                        name.val('');
                        number.val('');
                        img.attr("src", "/uniforms/base.png");
                        img.fadeIn(500);
                        $('.error').show();
                    }
                    else{
                        img.attr("src", "/_jersey/view/" +res);
                        img.fadeIn(1000);
                        $('.personal-target p').hide();
                        $("#download-share").attr("href", url+ "_jersey/download/" +res);
                        $("#fb-share").attr("href", "http://www.facebook.com/sharer/sharer.php?s=100&p[url]=" + url + "_jersey/view/" + res + "&p[images][0]=&p[title]=&p[summary]=I%20got%20my%20digital%20%40columbuscrew%20jersey%20from%20the%20club%2C%20and%20I%20can%27t%20wait%20to%20sit%20in%20my%20season%20ticket%20seats%21");
                        $("#twt-share").attr("href", "http://twitter.com/share?text=I%20got%20my%20digital%20%40columbuscrew%20jersey%20and%20I%20can%27t%20wait%20to%20sit%20in%20my%20season%20ticket%20seats%21%20Create%20your%20own%20here:&url=" +url+ "_jersey/view/" +res);
                        $("#email-share").attr("href", "mailto:?subject=I got my digital Columbus Crew jersey from the club, and I can't wait to sit in my season ticket seats!&body=" + url+ "_jersey/view/" +res);
                        $('.error').hide();
                        }
                    }
                });
            }
        });

        $('.error').click(function() {
            $('.error').hide();
        });
        img.click(function() {
            $('.error').hide();
        });
        closebg.click(function () {
            $('.error').hide();
            $('.personal-close').hide();
            closebg.fadeOut();
            frame.fadeOut();
            img.fadeOut();
        });
        $('.personal-close').click(function () {
            $('.error').hide();
            $('.personal-close').hide();
            closebg.fadeOut();
            frame.fadeOut();
            img.fadeOut();
        });
        $('.personal-share *').click(function(e) {
            e.stopPropagation();
        });
        $('.personal-share-link').click(function() {
            var personalizeJerseyLink = $('.personal-share-link');
            $('.error').hide();
            if ($('.personal-share').css('right') < '0') {
                personalizeJerseyLink.css('background', 'url("../images/buttons/pixel.png") no-repeat');
                $('.personal-share').animate({'right': '0'}, 500);
            } else {
                personalizeJerseyLink.css('background', 'url("../images/buttons/PersonalizeYourJersey-nonactive.png") no-repeat');
                $('.personal-share').animate({'right': '-710'}, 500);
            }
        });
        $('.personal-download').click(function() {
            var dl = $('#person-img').attr("src");
            dl = dl.replace('view','download');
            window.open(dl);
        });
        shareLaunch.click(function() {
            var shareNameVal = shareName.val();
            var shareNumberVal = shareNumber.val();
            if(shareNumber == ''){
                shareNumber.focus();
            }
            if(shareName == ''){
                shareName.focus();
            }
            else{
                img.fadeOut();
                $.ajax({
                  type: "GET",
                  url: "/_jersey/generate",
                  data: {
                        name: shareNameVal,
                        number: shareNumberVal
                    },
                success: function(res) {
                    if(res == 'error'){
                        shareName.val('');
                        shareNumber.val('');
                        img.attr("src", "/uniforms/base.png");
                        img.fadeIn(500);
                        $('.error').show();
                    }
                    else{
                        img.attr("src", "/_jersey/view/" +res);
                        $('.personal-share').animate({'right': '-710'}, 200);
                        img.fadeIn(1000);
                        $('.personal-target p').hide();
                        $("#download-share").attr("href", url+ "_jersey/download/" +res);
                        $("#fb-share").attr("href", "http://www.facebook.com/sharer.php?u=" +url+ "_jersey/view/" +res);
                        $("#twt-share").attr("href", "http://twitter.com/share?text=I%20got%20my%20digital%20%40columbuscrew%20jersey%20from%20the%20club%2C%20and%20I%20can%27t%20wait%20to%20sit%20in%20my%20season%20ticket%20seats%21%20Create%20your%20own%20here:&url=" +url+ "_jersey/view/" +res);
                        $("#email-share").attr("href", "mailto:?subject=I got my digital Columbus Crew jersey from the club, and I can't wait to sit in my season ticket seats!&body=" + url+ "_jersey/view/" +res);
                        $('.error').hide();
                        img.fadeIn();
                        }
                    }
                });
            }
        });
    }());

    $('.popup-catch').height($.getDocHeight()+100);
    $('.popup-catch').width($.getDocWidth());
});

$(window).resize(function () {
    $('.popup-catch').height($.getDocHeight()+100);
    $('.popup-catch').width($.getDocWidth());
});
$.fn.scrollBottom = function() {
    return $(document).height() - this.scrollTop() - this.height();
};

function jwgo(){
    $('body').find('.jwplayer').each(function() {
        videoName = $(this).attr('id');
        var config = {
            events: {
                onComplete: function(){
                    //goHome();
                }
            },
            autoplay: 'false',
            width: '731',
            height: '413',
            'controlbar.position': "over",
            'controlbar.idlehide': "true",
            volume: '100',
            icons: false,
            bufferlength: '15',
            image: '../images/videos/' + videoName + '.jpg',
            modes: [
                { type: 'html5' },
                { type: 'flash', src: '../jwplayer/player.swf' }
            ],
            levels: [
                {file: "http://12f26de991fde900e2d9-77526bc3d7707629c0ffeb2dcc8fa310.r12.cf1.rackcdn.com/crew/2014/" + videoName + ".mp4"},
                {file: "http://12f26de991fde900e2d9-77526bc3d7707629c0ffeb2dcc8fa310.r12.cf1.rackcdn.com/crew/2014/" + videoName + ".webm"}]
        };
        jwplayer(videoName).setup(config);
    });
}