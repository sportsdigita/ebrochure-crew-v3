st = (function() {
    // var prefix = "2013 Season Ticket Plans";
    // s.channel = "eBrochure";
    // s.prop1 = "2013 White Sox Season Ticket Plans eBrochure";
    // if (window.segment && window.segment == 'premium') {
    //     s.prop1 = "2013 White Sox Premium Seating Options eBrochure";
    //     prefix = "2013 Premium Seating Options";
    // }
    // if (window.segment && window.segment == 'groups') {
    //     s.prop1 = "2013 White Sox Group & Party Areas eBrochure";
    //     prefix = "2013 Group & Party Areas";
    // }
    // s.prop24 = s_rsid;

    // s.events = "event4";

    // s.campaign = s.getQueryParam("partnerId");
    // s.eVar1 = s.getQueryParam("vbID");
    // s.eVar4 = s.getQueryParam("affiliateId");
    // s.eVar14 = s.getQueryParam("tcid"); 
    // s.eVar21 = s.getQueryParam("tdl");
    // s.eVar22 = s.getQueryParam("tfl");
    // s.eVar25 = s.getQueryParam("mlbkw");

    // s.linkInternalFilters="javascript:,mlb,info-mlb";

    return function(n) {
        _gaq.push(['_trackEvent', 'Tabs', 'Click', n]);
        // s.pageName = "Chicago White Sox: eBrochure: " + prefix + ": " + n;
        // var s_code = s.t();
        // if (s_code) {
        //     document.write(s_code);
        // }
    };
})();
// delete s;

function stc(pageName) {
    return function() { st(pageName); };
};


$(document).ready(function () {
    $('.tab-join').click(        stc('Choose Your Ticket Plan'));
    $('.tab-stanleycup').click(  stc('2013 Stanley Cup Playoffs'));
    $('.tab-realignment').click( stc('Realignment'));
    $('.tab-benefits').click(    stc('Benefits'));
    $('.tab-rewards').click(     stc('Wild Rewards'));
    $('.tab-ourstory').click(    stc('Our Best Days Ahead'));
    $('.tab-custom').click(      stc('Custom Tab'));



    // $('.buy-now-header-link').click(       stc('Buy Now'));

    // if (window.segment == 'season') {
    //     // buy now

    //     $('.tab-fullseason').click(        stc('Full Season Plans'));
    //     $('.season1a .one').click(         stc('Full Season Plans - More Info'));

    //     $('.tab-splitseason').click(       stc('Split Season Plans'));
    //     $('.season2a .one').click(         stc('Split Season Plans - Hit and Run Plan'));
    //     $('.season2a .two').click(         stc('Split Season Plans - Double Play Plan'));

    //     $('.tab-pick14').click(            stc('Pick 14 Plan'));
    //     $('.season3a .one').click(         stc('Pick 14 Plan - More Info'));

    //     $('.tab-pick7').click(             stc('Pick 7 Plan'));
    //     $('.season4a .one').click(         stc('Pick 7 Plan - Buy Now'));

    //     $('.tab-benefits').click(          stc('Benefits and Incentives'));
    //     $('.season5a .one').click(         stc('Benefits and Incentives - Buy Now'));
    //     // more info

    //     $('.tab-stadiumclub').click(       stc('Stadium Club'));
    //     $('.season6a .one').click(         stc('Stadium Club - More Info'));

    //     $('.tab-seating').click(           stc('Seating and Pricing'));
    // }

    // if (window.segment == 'groups') {
    //     $('.tab-group').click(             stc('Group Tickets'));
    //     $('.groups1a .one').click(         stc('Group Tickets - More Info'));
        
    //     $('.tab-diamond').click(           stc('Diamond Suites'));
    //     $('.groups2a .one').click(         stc('Diamond Suites - More Info'));
        
    //     $('.tab-patiowarningtrack').click( stc('Patio & Warning Track Parties'));
    //     $('.groups3a .one').click(         stc('Patio & Warning Track Parties - More Info'));

    //     $('.tab-privatebank').click(       stc('The Privatebank Fan Deck'));
    //     $('.groups4a .one').click(         stc('The Privatebank Fan Deck - More Info'));

    //     $('.tab-fundraisers').click(       stc('Fundraisers'));
    //     $('.groups5a .one').click(         stc('Fundraisers - More Info'));

    //     $('.tab-millerlite').click(        stc('Miller Lite Party Porch'));
    //     $('.groups6a .one').click(         stc('Miller Lite Party Porch - More Info'));

    //     $('.tab-groupamenities').click(    stc('Group Amenities'));
    //     $('.groups7a .one').click(         stc('Group Amenities - More Info'));
    // }

    // if (window.segment == 'premium') {
    //     $('.tab-benefits').click(          stc('Premium Seating Benefits'));
    //     $('.premium1a .one').click(        stc('Premium Seating Benefits - More Info'));

    //     $('.tab-scout').click(             stc('Magellan Scout Seats'));
    //     $('.premium2a .one').click(        stc('Magellan Scout Seats - More Info'));
    //     $('.premium2a .two').click(        stc('Magellan Scout Seats - Pricing'));

    //     $('.tab-gold').click(              stc('Gold Coast Tickets Club'));
    //     $('.premium3a .one').click(        stc('Gold Coast Tickets Club - More Info'));
    //     $('.premium3a .two').click(        stc('Gold Coast Tickets Club - Pricing'));

    //     $('.tab-leased').click(            stc('Leased Suites'));
    //     $('.premium4a .one').click(        stc('Leased Suites - More Info'));
    //     $('.premium4a .two').click(        stc('Leased Suites - Pricing'));
    // }
});
