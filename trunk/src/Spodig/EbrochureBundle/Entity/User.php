<?php
// src/Spodig/EbrochureBundle/Entity/User.php

namespace Spodig\EbrochureBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\GroupInterface as GroupInterface;
use FOS\UserBundle\Model\User as AbstractUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    public $isadmin = null;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    protected $email;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->title = 'Sales Representative';

        // At a minimum, make sure all new users are sales people
        $this->addRole('ROLE_SALES');
    }
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var string $phone_number
     */
    private $phone_number;

    /**
     * @var string $slug
     */
    private $slug;
    private $title;
    private $photo;
    public $photo_file;
    private $chat;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {   
        if (!$this->name) {
            $this->name = "";
        }
        return $this->name;
    }

    /**
     * Set phone_number
     *
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phone_number = $phoneNumber;
        return $this;
    }

    /**
     * Get phone_number
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }
    
    /**
     * Get fax_number
     *
     * @return string 
     */
    public function getFaxNumber()
    {
        return '202.527.7941';
    }
    
    /**
     * Get user_photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    public function getPhotoUrl()
    {
        return '';
    }

    public function getRelativePath()
    {
        if (file_exists($this->getUploadRootDir() . $this->getPhoto())) {
            return $this->photo;
        }
        return $this->id . '/' . $this->photo;
    }

    public function getAbsolutePath()
    {
        return null === $this->getRelativePath() ? null : $this->getUploadRootDir() . '/' . $this->getRelativePath();
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'images/rep_photos/';
    }

    public function getAssetPath() {
        return $this->getUploadDir() . $this->getRelativePath();
    }

    public function upload($basepath)
    {
        if (null === $this->photo_file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        $userPath = $this->getUploadRootDir($basepath) . $this->id;
        if (!file_exists($userPath)) {
            mkdir($userPath);
        }

        $this->photo_file->move($userPath, $this->photo_file->getClientOriginalName());
        $this->setPhotoFromFile();
        $this->photo_file = null;
    }

    public function setPhotoFromFile()
    {
        if (null === $this->photo_file) {
            return;
        }

        $this->setPhoto($this->photo_file->getClientOriginalName());

        return $this;
    }

    /**
     * Set slug
     *
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @var Spodig\EbrochureBundle\Entity\Business
     */
    private $businesses;


    /**
     * Add businesses
     *
     * @param Spodig\EbrochureBundle\Entity\Business $businesses
     */
    public function addBusiness(\Spodig\EbrochureBundle\Entity\Business $businesses)
    {
        $this->businesses[] = $businesses;
    }

    /**
     * Get businesses
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getBusinesses()
    {
        return $this->businesses;
    }
    
    /**
     * To string
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setChat($chat)
    {
        $this->chat = $chat;
        return $this;
    }
    
    public function getChat()
    {
        return $this->chat;
    }
    
    public function convertIsAdmin()
    {
        $this->setEnabled(true);
        if ($this->isadmin === NULL)
            return;
        if ($this->isadmin === true)
        {
            $this->addRole('ROLE_SALES');
            $this->addRole('ROLE_ADMIN');
        }
        else
        {
            $this->removeRole('ROLE_ADMIN');
            $this->addRole('ROLE_SALES');
        }
    }
}
