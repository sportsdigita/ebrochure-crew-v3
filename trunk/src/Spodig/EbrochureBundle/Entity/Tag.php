<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 */
class Tag
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $hrId;

    /**
     * @var string
     */
    private $name;

    private $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hrId
     *
     * @param integer $hrId
     * @return Tag
     */
    public function setHrId($hrId)
    {
        $this->hrId = $hrId;

        return $this;
    }

    /**
     * Get hrId
     *
     * @return integer
     */
    public function getHrId()
    {
        return $this->hrId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getContacts()
    {
        return $this->contacts->toArray();
    }
}
