<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Spodig\EbrochureBundle\Entity\Renewal
 */
class Renewal
{
    /**
     * @var integer $id
     */
    protected $id;
    private $payment;
    private $playoff;
    private $addtl_playoff;
    private $customer;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }

    public function getPlayoff()
    {
        return $this->playoff;
    }

    public function setPlayoff($playoff)
    {
        $this->playoff = $playoff;
        return $this;
    }

    public function getAddtlPlayoff()
    {
        return $this->addtl_playoff;
    }

    public function setAddtlPlayoff($addtl_playoff)
    {
        $this->addtl_playoff = $addtl_playoff;
        return $this;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    public function getCustomer()
    {
        return $this->customer;
    }
}