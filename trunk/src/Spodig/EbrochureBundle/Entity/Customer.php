<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class Customer extends AbstractCustomer
{
    protected $id;
    private $first_name;
    private $last_name;
    private $start_year;
    private $membership_level;
    private $section;
    private $row;
    private $seats;
    private $num_seats;
    private $price_per_seat;
    private $plan_code;
    private $pc;
    private $total;
    private $final_total;
    private $alt_name;
    private $decision_maker;
    private $tab_segment;
    private $renewal;
    private $logo;
    public $logo_file;
    public $remove_logo;
    private $full_csv;

    private $plans;

    public function __construct()
    {
        parent::__construct();
        $this->plans = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getDisplayName()
    {
        if ($this->first_name || $this->last_name) {
            return $this->first_name . ' ' . $this->last_name;
        }
        return null;
    }

    /**
     * Accessors
     * Everything below here is an accessor with no logic.
     *
     * "set" methods MUST return $this
     */

    public function setPlanCode($plan_code)
    {
        $this->plan_code = $plan_code;
        return $this;
    }

    public function getPlanCode()
    {
        return $this->plan_code;
    }

    public function setAltName($alt_name)
    {
        $this->alt_name = $alt_name;
        return $this;
    }

    public function getAltName()
    {
        return $this->alt_name;
    }

    public function setDecisionMaker($decision_maker)
    {
        $this->decision_maker = $decision_maker;
    }

    public function getDecisionMaker()
    {
        return $this->decision_maker;
    }

    public function setFinalTotal($final_total)
    {
        $this->final_total = $final_total;
        return $this;
    }

    public function getFinalTotal()
    {
        return $this->final_total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setPc($pc)
    {
        $this->pc = $pc;
        return $this;
    }

    public function getPc()
    {
        return $this->pc;
    }

    public function setMembershipLevel($membership_level)
    {
        $this->membership_level = $membership_level;
        return $this;
    }

    public function getMembershipLevel()
    {
        return $this->membership_level;
    }

    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function setSeats($seats)
    {
        $this->seats = $seats;
        return $this;
    }

    public function getSeats()
    {
        return $this->seats;
    }

    public function setNumSeats($num_seats)
    {
        $this->num_seats = $num_seats;
        return $this;
    }

    public function getNumSeats()
    {
        return $this->num_seats;
    }

    public function setPricePerSeat($price_per_seat)
    {
        $this->price_per_seat = $price_per_seat;
        return $this;
    }

    public function getPricePerSeat()
    {
        return $this->price_per_seat;
    }


    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
        return $this;
    }

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
        return $this;
    }

    public function getLastName()
    {
        return $this->last_name;
    }


    public function setStartYear($startYear)
    {
        $this->start_year = $startYear;
        return $this;
    }

    public function getStartYear()
    {
        return $this->start_year;
    }

    public function getTabSegment()
    {
        return $this->tab_segment;
    }

    public function setTabSegment($tab_segment)
    {
        $this->tab_segment = $tab_segment;
        return $this;
    }

    public function getRenewal()
    {
        return $this->renewal;
    }

    public function setRenewal($renewal)
    {
        $this->renewal = $renewal;
        return $this;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setRemoveLogo($logo)
    {
        if ($logo == null) {
            return;
        }
        else {
            $this->logo = null;
            return $this;
        }
    }

    public function getRelativePath()
    {
        return $this->id . '/' . $this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->getRelativePath() ? null : $this->getUploadRootDir().'/'.$this->getRelativePath();
    }

    public function getWebPath()
    {
        if (file_exists($this->getUploadRootDir() . '/' . $this->getLogo())) {
            return $this->getUploadDir() . '/' . $this->getLogo();
        }
        return $this->getUploadDir() . '/' . $this->getRelativePath();
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it's correct when displaying uploaded doc/image in the view.
        return 'images/custom_logos/';
    }

    public function upload($basepath)
    {
        // the file property can be empty if the field is not required
        if (null === $this->logo_file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        $businessPath = $this->getUploadRootDir($basepath) . $this->id;
        if (!file_exists($businessPath)) {
            mkdir($businessPath);
        }
        $this->logo_file->move($businessPath, $this->logo_file->getClientOriginalName());

        $this->setLogoFromFile();

        // clean up the file property as you won't need it anymore
        $this->logo_file = null;
    }

    public function setLogoFromFile()
    {
        if (null === $this->logo_file) {
            return;
        }

        // set the path property to the filename where you'ved saved the file
        $this->setLogo($this->logo_file->getClientOriginalName());

        return $this;
    }

    public function getFullCsv()
    {
        return $this->full_csv;
    }

    public function setFullCsv($full_csv)
    {
        $this->full_csv = $full_csv;
        return $this;
    }


    /******PLANS*******/
    public function addPlan(Plan $plan)
    {
        $this->plans[] = $plan;
        return $this;
    }

    public function getPlans()
    {
        return $this->plans;
    }

    public function setPlans($plans)
    {
        if (!($plans instanceof ArrayCollection))
        {
            $plans = new ArrayCollection($plans);
        }

        $this->plans = $plans;
        return $this;
    }

}
