<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Spodig\EbrochureBundle\Entity;

class Plan
{
    protected $id;
    protected $customer;
    protected $num_seats;
    protected $plan_code;
    protected $price_per_seat;
    protected $row;
    protected $seats;
    protected $section;
    protected $total;

    public function getId()
    {
        return $this->id;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    public function setPlanCode($plan_code)
    {
        $this->plan_code = $plan_code;
        return $this;
    }

    public function getPlanCode()
    {
        return $this->plan_code;
    }

    public function setAltName($alt_name)
    {
        $this->alt_name = $alt_name;
        return $this;
    }

    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    public function getSection()
    {
        return $this->section;
    }

    public function setRow($row)
    {
        $this->row = $row;
        return $this;
    }

    public function getRow()
    {
        return $this->row;
    }

    public function setSeats($seats)
    {
        $this->seats = $seats;
        return $this;
    }

    public function getSeats()
    {
        return $this->seats;
    }

    public function getSeatQuantity()
    {
        $seat_ex = explode("-", $this->seats);

        $seat_count = $seat_ex[1]-$seat_ex[0]+1;

        return $seat_count;
    }

    public function setNumSeats($num_seats)
    {
        $this->num_seats = $num_seats;
        return $this;
    }

    public function getNumSeats()
    {
        return $this->num_seats;
    }

    public function setPricePerSeat($price_per_seat)
    {
        $this->price_per_seat = $price_per_seat;
        return $this;
    }

    public function getPricePerSeat()
    {
        return $this->price_per_seat;
    }

    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    public function getTotal()
    {
        return $this->total;
    }
}