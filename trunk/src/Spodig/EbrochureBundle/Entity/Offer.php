<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class Offer
{
    private $id;
    private $name;
    private $twig;
    private $slug;
    private $url;
    private $tab_image;
    private $alt_text;
    private $main_image;
    private $view;
    private $sort;
    private $location;

    public function getId()
    {
        return $this->id;
    }


    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setTwig($twig)
    {
        $this->twig = $twig;
    }

    public function getTwig()
    {
        return $this->twig;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function setTabImage($tabImage)
    {
        $this->tab_image = $tabImage;
    }

    public function getTabImage()
    {
        return $this->tab_image;
    }

    public function setAltText($altText)
    {
        $this->alt_text = $altText;
    }

    public function getAltText()
    {
        return $this->alt_text;
    }

    public function setMainImage($mainImage)
    {
        $this->main_image = $mainImage;
    }

    public function getMainImage()
    {
        return $this->main_image;
    }

    public function setView($view)
    {
        $this->view = $view;
    }

    public function getView()
    {
        return $this->view;
    }

    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    public function getSort()
    {
        return $this->sort;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function getLocation()
    {
        return $this->location;
    }
}
