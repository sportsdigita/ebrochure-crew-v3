<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractCustomer
{
    protected $id;
    private $slug;
    private $segment;
    private $offers;
    private $other_offers;
    private $address_1;
    private $address_2;
    private $city;
    private $state;
    private $zip;
    private $email;
    private $phone;
    private $account_id;
    private $first_tab;
    private $sales_rep;
    private $tabs;
    private $custom_title;
    private $custom_content;
    private $views = 0;
    private $last_visited;
    private $created;
    private $page_title;
    private $custom_picture_title;
    private $custom_picture;
    private $view_notify;
    private $invoice_notify;
    private $organization;
    private $sale_made;

    private $id2;

    public $custom_picture_file;

    abstract public function getName();

    public function __toString()
    {
        return $this->getName();
    }

    public function __construct()
    {
        $this->offers = new ArrayCollection();
        $this->tabs = new ArrayCollection();
        $this->created = new \DateTime('now');
    }

    private function getWebPath()
    {
        return __DIR__ . '/../../../../web';
    }

    private function getCustomPicturePath()
    {
        return 'images/custom_tab_pictures';
    }

    public function getRandomOffer()
    {
        $numberOfOffers = count($this->offers);
        if ($numberOfOffers == 1) {
            $randomOffer = 0;
        } else {
            $randomOffer = rand(0, $numberOfOffers - 1);
        }
        $offer = $this->offers[$randomOffer];
        return $offer;
    }

    public function setCustomPictureFromUpload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->custom_picture_file) {
            return;
        }

        $path = $this->getWebPath() . '/' . $this->getCustomPicturePath();
        if (!file_exists($path)) {
            mkdir($path);
        }

        $uniq = uniqid('', true);

        $this->custom_picture_file->move($path, $uniq .$this->custom_picture_file->getClientOriginalName());

        $this->setCustomPicture($uniq . $this->custom_picture_file->getClientOriginalName());

        return $this;
    }

    public function getCustomPictureAssetUrl()
    {
        if (!$this->custom_picture) {
            return '';
        }

        return $this->getCustomPicturePath() . '/' . $this->custom_picture;
    }

    /**
     * Accessors
     * Everything below here is an accessor with no logic.
     *
     * "set" methods MUST return $this
     */

    public function setOrganization($organization)
    {
        $this->organization = $organization;
        return $this;
    }

    public function getOrganization()
    {
        return $this->organization;
    }

    public function getLastVisited()
    {
        return $this->last_visited;
    }

    public function setCustomPictureTitle($custom_picture_title)
    {
        $this->custom_picture_title = $custom_picture_title;
        return $this;
    }

    public function getCustomPictureTitle()
    {
        return $this->custom_picture_title;
    }

    public function setCustomPicture($custom_picture)
    {
        $this->custom_picture = $custom_picture;
        return $this;
    }

    public function getCustomPicture()
    {
        return $this->custom_picture;
    }

    public function getPageTitle()
    {
        return $this->page_title;
    }

    public function setPageTitle($page_title)
    {
        $this->page_title = $page_title;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getViews()
    {
        return $this->views;
    }

    public function incrementViews()
    {
        ++$this->views;
        return $this;
    }

    public function updateLastVisited()
    {
        $this->last_visited = new \DateTime('now');
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSegment($segment)
    {
        $this->segment = $segment;
        return $this;
    }

    public function getSegment()
    {
        return $this->segment;
    }

    public function getTabs($sorted = false)
    {
        if($sorted)
        {
            $tabs = $this->tabs->toArray();

            usort($tabs, function($a, $b){
                /** @var $a Tab */
                /** @var $b Tab */
                if($a->getSortOrder() > $b->getSortOrder())
                    return 1;

                if($b->getSortOrder() < $a->getSortOrder())
                    return -1;

                return 0;
            });

            return $tabs;
        }

        return $this->tabs;
    }

    public function addTab($tab)
    {
        $this->tabs[] = $tab;
        return $this;
    }

    public function setTabs($tabs) {
        if (!($tabs instanceof ArrayCollection)) {
            $tabs = new ArrayCollection($tabs);
        }
        $this->tabs = $tabs;
        return $this;
    }

    public function addOffer(Offer $offer)
    {
        $this->offers[] = $offer;
        return $this;
    }

    public function getOffers()
    {
        return $this->offers;
    }

    public function getOtherOffers()
    {
        return $this->offers;
    }

    public function setAddress1($address1)
    {
        $this->address_1 = $address1;
        return $this;
    }

    public function getAddress1()
    {
        return $this->address_1;
    }

    public function setAddress2($address2)
    {
        $this->address_2 = $address2;
        return $this;
    }

    public function getAddress2()
    {
        return $this->address_2;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setState($state)
    {
        $this->state = $state;
        return $this;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
        return $this;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setEmail($email)
    {
        $this->email = strtolower($email);
        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setAccountId($accountId)
    {
        $this->account_id = $accountId;
        return $this;
    }

    public function getAccountId()
    {
        return $this->account_id;
    }

    public function setSalesRep(User $salesRep)
    {
        $this->sales_rep = $salesRep;
        return $this;
    }

    public function getSalesRep()
    {
        return $this->sales_rep;
    }

    public function getFirstTab()
    {
        return $this->first_tab;
    }

    public function setFirstTab(Tab $ft)
    {
        $this->first_tab = $ft;
        return $this;
    }

    public function setCustomTitle($title)
    {
        $this->custom_title = $title;
        return $this;
    }

    public function getCustomTitle()
    {
        return $this->custom_title;
    }

    public function setCustomContent($content)
    {
        $this->custom_content = $content;
        return $this;
    }

    public function getCustomContent()
    {
        return $this->custom_content;
    }

    public function getViewNotify()
    {
        return $this->view_notify;
    }

    public function setViewNotify($view_notify)
    {
        $this->view_notify = $view_notify;
        return $this;
    }

    public function getInvoiceNotify()
    {
        return $this->invoice_notify;
    }

    public function setInvoiceNotify($invoice_notify)
    {
        $this->invoice_notify = $invoice_notify;
        return $this;
    }

    public function getSaleMade()
    {
        return $this->sale_made;
    }

    public function setSaleMade($sale_made)
    {
        $this->sale_made = $sale_made;
        return $this;
    }
}
