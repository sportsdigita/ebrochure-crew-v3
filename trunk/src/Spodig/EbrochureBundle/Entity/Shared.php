<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\Group as BaseGroup;

/**
 * Spodig\EbrochureBundle\Entity\Shared
 */
class Shared
{
    /**
     * @var integer $id
     */
    protected $id;

    private $name;
    private $email;
    private $phone;
    private $contact;
    private $source;

    private $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }
}
