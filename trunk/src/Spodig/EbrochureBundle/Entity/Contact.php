<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 */
class Contact
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $hrId;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $company;

    /**
     * @var string
     */
    private $title;

    /**
     * @var purl
     */
    private $purl;

    private $email_views;
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hrId
     *
     * @param integer $hrId
     * @return Contact
     */
    public function setHrId($hrId)
    {
        $this->hrId = $hrId;

        return $this;
    }

    /**
     * Get hrId
     *
     * @return integer
     */
    public function getHrId()
    {
        return $this->hrId;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Contact
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Contact
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Contact
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set purl
     *
     * @param string $purl
     * @return Contact
     */
    public function setPurl($purl)
    {
        $this->purl = $purl;
        return $this;
    }

    /**
     * Get purl
     *
     * @return string
     */
    public function getPurl()
    {
        return $this->purl;
    }

    public function getEmailViews()
    {
        return $this->email_views;
    }

    public function addEmailViews($email_views)
    {
        $email_views->setContactId($this->id);

        $this->email_views = $email_views;
        return $this;
    }

    public function setTags(array $tags)
    {
        $this->tags = new ArrayCollection($tags);

        return $this;
    }

    public function getTags()
    {
        return $this->tags->toArray();
    }
}
