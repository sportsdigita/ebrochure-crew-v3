<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class Tab
{
    private $id;
    private $title;
    private $short_name;
    private $sort_order;
    private $segment;
    private $standard;
    private $parent;
    public $children;

    public function __construct()
    {
        // Defaults
        $this->brochure_version = 'consumer';
        $this->sort_order = 0;
    }

    public function __toString()
    {
        $visible = "{$this->title}";
        return $visible;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setShortName($sn)
    {
        $this->short_name = $sn;
        return $this;
    }

    public function getShortName()
    {
        return $this->short_name;
    }

    public function setSortOrder($order)
    {
        $this->sort_order = $order;
        return $this;
    }

    public function getSortOrder()
    {
        return $this->sort_order;
    }

    public function setSegment($bv)
    {
        $this->segment = $bv;
        return $this;
    }

    public function getSegment()
    {
        return $this->segment;
    }

    public function setStandard($bool)
    {
        $this->standard = $bool;
        return $this;
    }

    public function getStandard()
    {
        return $this->standard;
    }
}
