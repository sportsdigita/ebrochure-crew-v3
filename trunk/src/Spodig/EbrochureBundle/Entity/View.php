<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class View
{
    protected $id;
    private $time;
    private $ip_address;
    private $title;
    private $contact;

    public function __construct()
    {
        $this->time = new \DateTime('now');
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTime(\DateTime $time)
    {
        $this->time = $time;
        return $this;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this->title();
    }

    public function setIpAddress($ip_address)
    {
        $this->ip_address = $ip_address;
        return $this;
    }

    public function getIpAddress()
    {
        return $this->ip_address;
    }

    public function setContact($contact)
    {
        $this->contact = $contact;
        return $this;
    }

    public function getContact()
    {
        return $this->contact;
    }
}
