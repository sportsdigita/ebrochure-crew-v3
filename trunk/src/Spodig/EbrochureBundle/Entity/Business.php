<?php

namespace Spodig\EbrochureBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

class Business extends AbstractCustomer
{
    protected $id;
    private $name;
    private $logo;
    public $logo_file;
    private $decision_maker;
    public $tab_segment;
    public $remove_logo;

    public function __construct()
    {
        parent::__construct();
        $this->segment = 'business';
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        if (!$this->name) {
            return $this->getSlug();
        }
        return $this->name;
    }

    public function getDisplayName()
    {
        return $this->name;
    }

    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    public function getLogo()
    {
        return $this->logo;
    }

    public function setRemoveLogo($logo)
    {
        if ($logo == null) {
            return;
        }
        else {
            $this->logo = null;
            return $this;
        }
    }

    public function getRandomOffer()
    {
        $numberOfOffers = count($this->offers);
        if ($numberOfOffers == 1) {
            $randomOffer = 0;
        } else {
            $randomOffer = rand(0, $numberOfOffers - 1);
        }
        $offer = $this->offers[$randomOffer];
        return $offer;
    }

    public function getRelativePath()
    {
        return $this->id . '/' . $this->logo;
    }

    public function getAbsolutePath()
    {
        return null === $this->getRelativePath() ? null : $this->getUploadRootDir().'/'.$this->getRelativePath();
    }

    public function getWebPath()
    {
        if (file_exists($this->getUploadRootDir() . '/' . $this->getLogo())) {
            return $this->getUploadDir() . '/' . $this->getLogo();
        }
        return $this->getUploadDir() . '/' . $this->getRelativePath();
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it's correct when displaying uploaded doc/image in the view.
        return 'images/custom_logos/';
    }

    public function upload($basepath)
    {
        // the file property can be empty if the field is not required
        if (null === $this->logo_file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        $businessPath = $this->getUploadRootDir($basepath) . $this->id;
        if (!file_exists($businessPath)) {
            mkdir($businessPath);
        }
        $this->logo_file->move($businessPath, $this->logo_file->getClientOriginalName());

        $this->setLogoFromFile();

        // clean up the file property as you won't need it anymore
        $this->logo_file = null;
    }

    public function setLogoFromFile()
    {
        if (null === $this->logo_file) {
            return;
        }

        // set the path property to the filename where you'ved saved the file
        $this->setLogo($this->logo_file->getClientOriginalName());

        return $this;
    }

    public function setDecisionMaker($decision_maker)
    {
        $this->decision_maker = $decision_maker;
    }

    public function getDecisionMaker()
    {
        return $this->decision_maker;
    }
}
