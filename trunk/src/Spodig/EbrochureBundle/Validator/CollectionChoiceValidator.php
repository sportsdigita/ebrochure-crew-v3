<?php
namespace Spodig\EbrochureBundle\Validator;

use Symfony\Component\Validator\Constraints\ChoiceValidator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Constraint;

class CollectionChoiceValidator extends ChoiceValidator
{
    public function isValid($value, Constraint $constraint)
    {
        if ($value instanceof ArrayCollection || $value instanceof PersistentCollection) {
            $value = $value->toArray();
        }

        return parent::isValid($value, $constraint);
    }
}

