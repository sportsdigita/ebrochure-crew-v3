<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class CustomerAdmin extends AbstractCustomerAdmin
{
    protected $baseRouteName = 'admin_customer';

    protected function getEntityName()
    {
        return 'Customer';
    }

    protected function configureCustomerFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('first_name', 'text', array('required' => false))
            ->add('last_name');
    }

    protected function validateCustomer(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('last_name')
                ->assertMaxLength(array('limit' => 80))
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }

    protected function configureCustomerDatagridFilters(DatagridMapper $datagridMapper)
    {

    }

    protected function configureCustomerListFields(ListMapper $listMapper) {
    }
}
