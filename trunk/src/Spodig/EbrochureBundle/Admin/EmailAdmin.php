<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class EmailAdmin extends Admin
{
    protected $baseRouteName = 'admin_email';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', null, [
                'help'  => 'Enter a short description for this email'

        ])
            ->add('type', null, [
                'help'  => 'For now, just enter "redirect" (without the quotes)'

        ])
            ->add('link', 'text', [
                'help'  => 'The link you want to share. (replaces ((link)) in your message)'

        ])
            ->add('subject', null, [
                'help'  => 'The email subject'

        ])
            ->add('body', 'textarea', [
                'help'  => "The message of your email. Use the following shortcodes to personalize your message:<br>
                            ((link)) - The link (clicks will be tracked)<br>
                            ((name)) - The recipient's name<br>
                            ((title)) - The recipient's title<br>
                            ((company)) - The recipient's company"
        ])
            ->add('tags', 'text', [
                'help'  => 'The Highrise tags to send the message to. Multiple tags must be separated by commas without spaces'

        ])
        //     ->add('send', null, [
        //         'help'  => 'Date and time to automatically send this message'

        // ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('type')
            ->add('created')
            ->add('send')
            ->add('sent')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('type')
            ->add('created')
            ->add('send')
            ->add('sent')
        ;
    }
}
