<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection as RouteCollection;

class BusinessAdmin extends AbstractCustomerAdmin
{
    protected $baseRouteName = 'admin_business';
        
    protected function getEntityName()
    {
        return 'Business';
    }
    
    /**
     * prePersist
     * Set logo reference
     * @param $business
     */
    public function prePersist($business)
    {
        parent::prePersist($business);
        $business->setLogoFromFile();
    }
    
    /**
     * postPersist
     * Convert uploaded logo to Image
     * @param $business
     */
    public function postPersist($business)
    {
        $this->saveFile($business);
    }
    
    /**
     * preUpdate
     * Convert uploaded logo to Image
     * @param $business
     */
    public function preUpdate($business)
    {
        parent::preUpdate($business);
        $this->saveFile($business);
    }
    
    /**
     * postRemove
     * Delete associated logo
     * @param $business
     */
    public function postRemove($business)
    {
        die("Don't do it yet!");
    }
    
    public function saveFile($business) {
        $basepath = $this->getRequest()->getBasePath();
        $business->upload($basepath);  
    }
 
    protected function configureRoutes(RouteCollection $collection)
    {
        // $collection->add('duplicate');
        $collection->add('purls', 'purls');
    }
    
    protected function configureCustomerFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('decision_maker')
            ->add('logo_file', 'file', array('required' => false))
            ->add('logo')
            ->add('remove_logo', 'checkbox', array('required' => false))
        ;
    }

    protected function configureCustomerDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('segment', 'doctrine_orm_choice', [], 'choice', [
                'choices' => $this->config->getCustomerType('Business')['segments'],
            ])
        ;
    }

    protected function configureCustomerListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('logo', 'image')
        ;
    }

    protected function validateCustomer(ErrorElement $errorElement, $object)
    {
    }
    
}
