<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

use FOS\UserBundle\Model\UserManagerInterface;

class UserAdmin extends Admin
{
    protected $baseRouteName = 'admin_user';

    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'SpodigEbrochureBundle:Admin:edit_user.html.twig';
        }

        return parent::getTemplate($name);
    }

    public function prePersist($user)
    {
        $user->setPhotoFromFile();
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
        $this->saveFile($user);
    }

    /**
     * @param UserManagerInterface $userManager
     */
    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }
    public function postPersist($user)
    {
        $this->saveFile($user);
    }

    public function saveFile($user)
    {
        $basepath = $this->getRequest()->getBasePath();
        $user->upload($basepath);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('plainPassword', 'password', array('required' => false, 'label' => 'Password'))
            ->add('name')
            ->add('email')
            ->add('phone_number')
            ->add('slug')
            ->add('chat')
            ->add('photo_file', 'file', ['required' => false])
            ->add('photo')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('name')
            ->add('slug')
            ->add('chat')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('name')
            ->add('slug')
            ->add('chat')
        ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('name')
                ->assertMaxLength(array('limit' => 150))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end()
            ->with('slug')
                ->assertMaxLength(array('limit' => 20))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end()
        ;
    }
}
