<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;

class OfferAdmin extends Admin
{

    protected $baseRouteName = 'admin_offer';

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('slug')
            ->add('url')
            ->add('main_image')
            ->add('tab_image')
            ->add('alt_text')
            ->add('view', 'choice', array(
                'choices' => array(
                    'season'   => 'Season Ticket Holders',
                    'consumer' => 'Business to Consumer',
                    'business' => 'Business to Business',
                )
            ))
            ->add('sort')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('slug')
            ->add('view')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('slug')
            ->add('view')
            ->add('sort')
        ;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('name')
                ->assertMaxLength(array('limit' => 150))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end()
            ->with('slug')
                ->assertMaxLength(array('limit' => 20))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end()
            ->with('url')
                ->assertMaxLength(array('limit' => 500))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->end()
        ;
    }
}