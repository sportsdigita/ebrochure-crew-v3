<?php

namespace Spodig\EbrochureBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

abstract class AbstractCustomerAdmin extends Admin
{
    protected $config;
    private $securityContext;
    private $doctrine;
    private $router;

    abstract protected function getEntityName();
    abstract protected function configureCustomerFormFields(FormMapper $formMapper);
    abstract protected function validateCustomer(ErrorElement $errorElement, $object);
    abstract protected function configureCustomerDatagridFilters(DatagridMapper $datagridMapper);
    abstract protected function configureCustomerListFields(ListMapper $listMapper);

    public function getTemplate($name)
    {
        if ($name == 'edit') {
            return 'SpodigEbrochureBundle:Admin:edit_customer.html.twig';
        }

        return parent::getTemplate($name);
    }

    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function setSecurityContext($sc)
    {
        $this->securityContext = $sc;
    }

    public function setRouter($router)
    {
        $this->router = $router;
    }

    public function prePersist($customer)
    {
        parent::prePersist($customer);
        $customer->setLogoFromFile();
        return $this->_preUpdate($customer);
    }

    public function postPersist($customer)
    {
        $this->saveFile($customer);
    }

    private function _preUpdate($user)
    {
        $user->setCustomPictureFromUpload();
        // Sales reps can only create customers with their own contact
        // information.
        if (    $this->securityContext->isGranted('ROLE_SALES')
            && !$this->securityContext->isGranted('ROLE_ADMIN'))
        {
            $user->setSalesRep($this->securityContext->getToken()->getUser());
        }

        $user->setSlug( preg_replace("[^_a-z0-9-]", '', strtolower($user->getSlug()) ) );
    }

    public function preUpdate($customer)
    {
        parent::preUpdate($customer);
        $this->saveFile($customer);
    }

    public function saveFile($customer) {
        $basepath = $this->getRequest()->getBasePath();
        $customer->upload($basepath);
    }

    public function getNewInstance()
    {
        $obj = parent::getNewInstance();
        $obj->setSegment($this->config->getCustomerType($this->getEntityName())['segments'][0]);
        return $obj;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {

        $segments = [];
        $segmentOptions = [];

		if (is_array($segments))
		{
       		foreach ($segments as $segment)
       		{
            	$segmentOptions[$segment] = ucwords($segment);
        	}
        }

        $choices = [];

        if ($this->config->getSegments()) {
            foreach ($this->config->getCustomerType($this->getEntityName())['segments'] as $segment) {
                $choices[$segment] = ucwords($segment);
            }
        }



        $formMapper->with($this->getEntityName() . ' Information');

        $formMapper
            ->add('email')
        ;

        $this->configureCustomerFormFields($formMapper);

        if ($this->config->hasFeature('extended_info')) {
            $formMapper
                ->add('account_id')
                ->add('phone')
                ->add('address1', 'text', array('required' => false))
                ->add('address2', 'text', array('required' => false))
                ->add('city')
                ->add('state')
                ->add('zip')
            ;
        }

        $formMapper
            ->add('slug', null, [
                'label' => 'URL Segment',
                'help'  => 'The link to the personalized brochure will be at: '
                        .  'SEGMENT'
                        .  '/[URL PART]'

        ]);

        if (count($choices) > 1) {
            $formMapper->add('segment', 'choice', [
                'choices' => $choices,
                'label'   => 'Brochure Version',
            ]);
        }
        else if (isset($choices[0])){
            $formMapper->add('segment', 'hidden', [
                'data'    => array_keys($choices)[0],
            ]);
        }
        else {
            $formMapper->add('segment', 'hidden', [
                'data'     => null,
            ]);
        }

        if ($this->config->hasFeature('custom_title')) {
            $formMapper->add('page_title');
        }

        if ($this->config->hasFeature('plan_types')) {
            $formMapper->add('plan_types');
        }

        if ($this->securityContext->isGranted('ROLE_ADMIN')) {
            $formMapper->add('sales_rep');
        }

        if ($this->config->hasFeature('track_sale_made')) {
            $formMapper
                ->add('sale_made', 'checkbox', [
                    'required' => false,
                    'label' => 'Sale Has Been Made'
            ]);
        }

        if ($this->config->hasFeature('custom_logo')) {
            $formMapper
                ->end()
                ->with('Logo Personalization');
            $formMapper
                ->add('logo_file', 'file', array('required' => false))
                ->add('logo')
                ->add('remove_logo', 'checkbox', array('required' => false))
                ->add('decision_maker')
            ;
        }

        if ($this->config->hasFeature('offers')) {
            $formMapper
                ->end()
                ->with('Offer Selection');

            $formMapper->add('offers', 'entity', [
                'required' => false,
                'class'    => 'Spodig\EbrochureBundle\Entity\Offer',
                'query_builder' => function (EntityRepository $er)
                {
                    return $er->createQueryBuilder( 'qb' )
                              ->add( 'select', 'm' )
                              // ->add( 'where', 'm.location = :identifier')
                              ->add( 'from', 'Spodig\EbrochureBundle\Entity\Offer m' )
                              // ->setParameter('identifier', 'right')
                              ;
                },
                'expanded' => true,
                'multiple' => true,
                'label'    => 'Popover Offers',
                'help'     => 'Choose one or more popover offers to be shown
                               to the customer.  If you choose more than one,
                               the offers will be cycled through every few
                               seconds.'
            ]);
        }

        if ($this->config->hasFeature('select_tabs')) {
            $formMapper
                ->end()
                ->with('Content Selection');

                $formMapper->add('tabs', null, [
                    'expanded' => true,
                    'help'     => 'You may choose up to 5 tabs to be included in the brochure.'
                ]);

            if ($this->config->hasFeature('custom_tabs')){
                $formMapper->add('custom_title','text',[
                    'required' => false,
                    'label' => 'Custom Tab Title',
                    'help' => 'If you selected the Custom Tab, enter your own title here.',
                    'max_length' => 40,
                ]);
                $formMapper->add('custom_content','textarea',array(
                    'required' => false,
                    'label' => 'Custom Tab Content',
                    'help'  => 'If you selected the Custom Tab, enter your own content here.',
                ));
            }

            if ($this->config->hasFeature('custom_picture_tab')) {
                $formMapper
                    ->add('custom_picture_title', 'text', [
                        'label' => 'Custom Picture Tab Title',
                    ])
                    ->add('custom_picture_file', 'file', [
                        'required' => false,
                        'help' => 'Choose an image file to show on the custom
                                   picture tab. The optimal size for this image is
                                   698x409.',
                    ])
                    ->add('custom_picture');
            }

            if ($this->config->hasFeature('first_tab')) {
                $formMapper->add('first_tab');
            }
        }

        $formMapper->end();

    }

    public function getFilterParameters()
    {
        if ($this->config->hasFeature('hide_foreign_purls')) {
            $this->datagridValues = array_merge($this->datagridValues, array(
                    'sales_rep' => array(
                        'value' => $this->securityContext->getToken()->getUser()->getId(),
                    )
                )
            );
        }
        $fp = parent::getFilterParameters();

        return $fp;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $this->configureCustomerDatagridFilters($datagridMapper);

        $datagridMapper
            ->add('slug', null, ['label' => 'PURL'])
            ->add('email')
            ->add('sales_rep')
        ;

        if ($this->config->hasFeature('track_sale_made')) {
            $datagridMapper
                ->add('sale_made');
        }

        if ($this->config->hasFeature('plan_types')) {
            $datagridMapper->add('plan_types');
        }
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $this->configureCustomerListFields($listMapper);
        $listMapper
            ->addIdentifier('name')
            ->add('purl', 'string', [
                'template' => 'SpodigEbrochureBundle:Admin:purl_list_field.html.twig',
                'label'    => 'PURL',
            ])
            ->add('views')
            ->add('created')
            ->add('email')
            ->add('segment')
            ->add('sales_rep')
        ;

        if ($this->config->hasFeature('track_sale_made')) {
            $listMapper
                ->add('sale_made');
        }
    }

    public function getExportFields()
    {
        $results = [
            'sales_rep',
            'account_id',
            'first_name',
            'last_name',
            'address_1',
            'address_2',
            'city',
            'state',
            'zip',
            'email',
            'phone',
            'views',
            'last_visited',
            'created'
        ];

        return $results;
    }

    public function validate(ErrorElement $errorElement, $object)
    {
        $this->validateCustomer($errorElement, $object);

        $tabs = $this->doctrine
            ->getRepository('SpodigEbrochureBundle:Tab')
            ->findAll();

        $offers = $this->doctrine
            ->getRepository('SpodigEbrochureBundle:Offer')
            ->findAll();

        $tabMethodName = 'assertSpodig\EbrochureBundle\Validator\CollectionChoice';

        $errorElement
            ->with('slug')
                ->assertMaxLength(array('limit' => 20))
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertRegex([
                    'pattern' => '/[^a-zA-Z0-9_\-]/',
                    'match' => false,
                    'message' => 'PURL may only contain letters, numbers, hyphens and underscores'
                ])
            ->end();
    }
}
