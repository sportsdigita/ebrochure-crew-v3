<?php

namespace Spodig\EbrochureBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Spodig\EbrochureBundle\Entity\Offer as Offer;

class OfferRepository extends EntityRepository
{
    /**
     * findOneRandom
     * @return Offer randomly selected offer
     */
    public function findOneRandom()
    {
        $offers = $this->findAll();
        $numberOfOffers = count($offers);
        if ($numberOfOffers == 1) {
            $randomOffer = 0;
        } else {
            $randomOffer = rand(0, $numberOfOffers - 1);
        }
        return $offers[$randomOffer];
    }
    
    /**
     * findOneRandomByView
     * @param string $view
     * @return Offer randomly selected offer
     */
    public function findOneRandomByView($view)
    {
        $offers = $this->findByView($view);
        $numberOfOffers = count($offers);
        if ($numberOfOffers == 1) {
            $randomOffer = 0;
        } else {
            $randomOffer = rand(0, $numberOfOffers - 1);
        }
        return $offers[$randomOffer];
    }

}