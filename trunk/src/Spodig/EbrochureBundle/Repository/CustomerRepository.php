<?php

namespace Spodig\EbrochureBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Spodig\EbrochureBundle\Entity\Customer as Customer;

class CustomerRepository extends EntityRepository
{
    /**
     * Find by email address or account id
     * @return Offer
     */
    public function findOneByEmailOrAccountId($emailAddress, $accountId)
    {
        $q = $this->getEntityManager()->createQuery("
            SELECT c FROM SpodigEbrochureBundle:Customer c 
            WHERE c.email = :email OR c.account_id = :account_id
        ")->setParameters(array(
            'email' => $emailAddress,
            'account_id' => $accountId,
        ));
        
        $customer = $q->getResult();

        return $customer[0];
    }

}