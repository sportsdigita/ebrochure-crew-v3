<?php

namespace Spodig\EbrochureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Spodig\EbrochureBundle\Entity\User as User;
use Spodig\EbrochureBundle\Entity\View as View;

class DefaultController extends Controller
{
    private function defaultTabs($segment)
    {
        return $this->getDoctrine()
            ->getRepository('SpodigEbrochureBundle:Tab')
            ->findBy(['standard' => true, 'segment' => $segment], ['sort_order' => 'ASC']);
    }

    public function segmentAction($segment, $purl)
    {
        $segments = $this->get('spodig_ebrochure.config')->getSegments();
        $domain = $this->container->getParameter('client_domain');
        $client = (!empty($domain)) ? $domain : 'aac';
        
        if (!empty($segment)) {
            $customerType = ucwords($segment);
        } else {
            $customerType = 'Business';
        }
        // dsaf
        if (!isset($segments[$segment])) {
            throw $this->createNotFoundException('Page not found.');
        }

        $em = $this->getDoctrine()->getManager();



        $customer = $em
            ->getRepository('SpodigEbrochureBundle:Customer')
            ->findOneBySlug($purl);

        if (!$customer) {

            $customer = $em->getRepository('SpodigEbrochureBundle:Business')->findOneBySlug($purl);

            if (!$customer) {
                $rep = $em
                    ->getRepository('SpodigEbrochureBundle:User')
                    ->findOneBySlug($purl);
                if (!$rep) {
                    $defaults = $this->getDoctrine()
                            ->getRepository('SpodigEbrochureBundle:Tab')
                            ->findBy(['standard' => true, 'segment' => $purl, 'parent' => null], ['sort_order' => 'ASC']);
                    foreach($defaults as $parent){
                        $parent->children= $this->getDoctrine()
                            ->getRepository('SpodigEbrochureBundle:Tab')
                            ->findBy(['standard' => true, 'segment' => $purl, 'parent' => $parent->getID()], ['sort_order' => 'ASC']);
                    }

                    //$tabs = $customer->getTabs(true);
                    $mytabs = '';
                    if($purl=='renew' || $purl=='acquisition'){
                        $mytabs = $this->defaultTabs($purl);
                        $segment = $purl;
                    } else {
                        $mytabs = $this->defaultTabs($segment);
                    }

                    return $this->render('::index.html.twig', array(
                        'client' => $client,
                        'segment'  => $segment,
                        'purl' => $purl,
                        'tabs' => $mytabs,
                    ));
                }
                return $this->render('::index.html.twig', [
                    'client' => $client,
                    'segment' => $segment,
                    'rep' => $rep,
                    'purl' => $purl,
                    'tabs' => $this->defaultTabs($segment)
                ]);
            }
        }

        if (!$this->get('security.context')->isGranted('ROLE_SALES')) {
            $previousViews = $customer->getViews();

            $customer->incrementViews();
            $customer->updateLastVisited();
            $em->persist($customer);
            $em->flush();

            $currentViews = $customer->getViews();

            try 
            {
                $emailNotifications = $this->get('spodig_ebrochure.config')->getParameter('email_notifications');
            } catch (Exception $e)
            {
                $emailNotifications = false;
            }

            if (empty($emailNotifications))
            {
                $unlimitedEmailNotifications = false;
            }
            elseif ($emailNotifications === true)
            {
                $unlimitedEmailNotifications = true;
            }
            else
            {
                $unlimitedEmailNotifications = false;
            }

            if (($previousViews === 0 && $currentViews === 1 && !$unlimitedEmailNotifications) || $unlimitedEmailNotifications)
            {
                $displayName = $customer->getDisplayName();
                $lastVisited = $customer->getLastVisited();
                $repName = $customer->getSalesRep()->getName();
                $repEmail = $customer->getSalesRep()->getEmail();
                $body = $this->renderView(
                    'SpodigEbrochureBundle:Default:view-notification-email.txt.twig',
                    [
                        'displayName' => $displayName,
                        'dateViewed' => $lastVisited->format('Y-m-d H:i:s')
                    ]
                );

                $message = \Swift_Message::newInstance()
                    ->setSubject('EBrochure View Notification')
                    ->setFrom(['info@sportsdigita.com' => 'Sportsdigita EBrochure'])
                    ->setTo(["$repEmail" => "$repName"])
                    ->setBody($body)
                ;

                $this->get('mailer')->send($message);
            }
        }

        $logo = false;
        if (is_callable([$customer, 'getLogo'])) {
            if ($customer->getLogo()) {
                if (file_exists(__DIR__ . '/../../../../web/images/custom_logos/' . $customer->getId() . '/' . $customer->getLogo())) {
                    $logo = '/images/custom_logos/' . $customer->getId() . '/' . $customer->getLogo();
                } else {
                    $logo = '/images/custom_logos/' . $customer->getLogo();
                }
            }
        }

        $tabs = $customer->getTabs(true);

        if (count($tabs) === 0) {
            $tabs = $this->defaultTabs($customer->getSegment());
        }

        $totalBalance = 0;
        $plans = $customer->getPlans();

        foreach ($plans as $plan)
        {
            $total = $plan->getTotal();
            $total = (int)$total;
            $totalBalance += $total;
        }

        $today = date("F j, Y");

        return $this->render('::index.html.twig', [
            'today' => $today,
            'purl' => $purl,
            'client' => $client,
            'segment' => $segment,
            'customer' => $customer,
            'plans' => $plans,
            'totalBalance' => $totalBalance,
            'customer_type' => $customerType,
            'offers' => $customer->getOffers(),
            'rep' => $customer->getSalesRep(),
            'first_tab' => $customer->getFirstTab(),
            'tabs' => $tabs,
            'logo' => $logo,
        ]);
    }
    /********END SEGMENT ACTION*********/

    public function purlAction($purl)
    {
        $em = $this->getDoctrine()->getManager();

        $customer = $em
            ->getRepository("SpodigEbrochureBundle:Customer")
            ->findOneBySlug($purl);

        if ($customer && $customer->getSegment()) {
            return $this->segmentAction($customer->getSegment(), $purl);
        }

        return $this->segmentAction(
            $this->get('spodig_ebrochure.config')->getParameter('default_segment'),
            $purl
        );
    }

    public function redirectAction() {
        $dest = $this->getRequest()->get('dest');
        return $this->render('::redirect.html.twig', [
            'redirect_url' => urldecode($dest),
        ]);
    }

    public function defaultAction()
    {
        $segment = $this->get('spodig_ebrochure.config')->getParameter('default_segment');
        $domain = $this->container->getParameter('client_domain');
        $client = (!empty($domain)) ? $domain : 'aac';

        $domain = $this->get('request')->getHost();

        if ($domain == 'myseats.wild.com') {
            $segment = 'full';
        } elseif ($domain == 'plans.wild.com') {
            $segment = 'partial';
        } elseif ($domain == 'myclubseats.wild.com') {
            $segment = 'club';
        }

        $defaults = $this->getDoctrine()
                ->getRepository('SpodigEbrochureBundle:Tab')
                ->findBy(['standard' => true, 'segment' => $segment, 'parent' => null], ['sort_order' => 'ASC']);
        foreach($defaults as $parent){
            $parent->children= $this->getDoctrine()
                ->getRepository('SpodigEbrochureBundle:Tab')
                ->findBy(['standard' => true, 'segment' => $segment, 'parent' => $parent->getID()], ['sort_order' => 'ASC']);
        }
        return $this->render('::index.html.twig', array(
            'client' => $client,
            'segment'  => $segment,
            'tabs' => $defaults,
        ));
    }
}
