<?php

//src/Spodig/EbrochureBundle/Controller/CustomerAdminController.php

namespace Spodig\EbrochureBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\Response;

class CustomerAdminController extends Controller
{
    public function tabsForSegmentAction($segment)
    {
        $tabs = $this->get('doctrine')
            ->getRepository('SpodigEbrochureBundle:Tab')
            ->findBy(['segment' => $segment], ['sort_order' => 'ASC']);

        $r = new Response();

        if (!$tabs) {
            $r->setContent('[]');
            return $r;
        }

        $output = [];
        $i = 0;
        foreach ($tabs as $tab) {
            $output[$i++] = [$tab->getShortName(), $tab->getTitle(), $tab->getId()];
        }

        $r->setContent(json_encode($output));

        return $r;
    }
}
