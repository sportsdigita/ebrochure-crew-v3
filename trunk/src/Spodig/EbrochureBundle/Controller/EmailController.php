<?php

namespace Spodig\EbrochureBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Spodig\EbrochureBundle\Entity\User as User;
use Spodig\EbrochureBundle\Entity\View as View;

class EmailController extends Controller
{
    public function interestAction(Request $request)
    {
        $firstName = $request->request->get('first-name');
        $lastName = $request->request->get('last-name');
        
        $fullName = "$firstName $lastName";

        $ticketPlan = $request->request->get('ticket-plan');
        $seatingLevel = $request->request->get('seating-level');
        $numberSeats = $request->request->get('number-seats');
        $otherQuestions = $request->request->get('other-questions', 'No other questions.');

        $recipientUser = $this->getDoctrine()->getManager()->getRepository('SpodigEbrochureBundle:User')->findOneById((int)$request->request->get('recipient'));
        $recipient = ($recipientUser == null || $recipientUser->getEmail() == null) ? 'seasontickets@dallasstars.com' : $recipientUser->getEmail();

        $sender = $request->request->get('email');

        $subject = '[Season Tickets Interest] - ' . $request->request->get('number-seats') . ' seat(s) at the ' . $request->request->get('seating-level') . ' seating level';
        $body = $this->renderView(
            'SpodigEbrochureBundle:Default:interest-email.txt.twig',
            [
                'name' => $fullName,
                'ticketPlan' => $ticketPlan,
                'seatingLevel' => $seatingLevel,
                'numberSeats' => $numberSeats,
                'questions' => $otherQuestions,
            ]
        );
        echo "$sender";
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($sender)
            ->setTo($recipient)
            ->setBody($body);

        $headers = $message->getHeaders();
        $headers->addTextHeader('X-SMTPAPI', '{"category": ["Season Tickets", "Dallas Stars"]}');

        $result = $this->get('mailer')->send($message);

        $state = ($result > 0) ? 'Success' : 'Failed';
        $code = ($result > 0) ? 200 : 400;

        $response = new Response();
        $response->setContent($state);
        $response->setStatusCode($code);
        $response->headers->set('Content-Type', 'text/html');

        return $response;
    }

    public function viewAction($purl, $title)
    {
        $em = $this->getDoctrine()->getManager();

        $recipient = $em
            ->getRepository('SpodigEbrochureBundle:Contact')
            ->findOneByPurl($purl);

        $email = $em
            ->getRepository('SpodigEbrochureBundle:Email')
            ->findOneByTitle($title);

        $email_view = new View;

        $email_view->setContact($recipient);
        $email_view->setTitle($title);
        $email_view->setIpAddress($this->get('request')->server->get('REMOTE_ADDR'));

        $em->persist($email_view);
        $em->flush();

        return $this->render('::view.html.twig', [
            'email' => $email,
            'recipient' => $recipient
        ]);
    }

    public function trackAction()
    {
        $em = $this->getDoctrine()->getManager();

        $views = $em
            ->getRepository('SpodigEbrochureBundle:View')
            ->findAll();

        return $this->render('::track.html.twig', [
            'views' => $views
        ]);
    }
}
