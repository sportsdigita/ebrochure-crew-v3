<?php

//src/Spodig/EbrochureBundle/Controller/BusinessAdminController.php

namespace Spodig\EbrochureBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;

class BusinessAdminController extends Controller
{
    /**
     * return the Response object associated to the create action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';
        
        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        $business = $this->admin->getNewInstance();

        $this->admin->setSubject($business);
        $business->setSalesRep($this->get('security.context')->getToken()->getUser());

        $form = $this->admin->getForm();
        $form->setData($business);

        if ($this->get('request')->getMethod() == 'POST') {
            $form->bindRequest($this->get('request'));
            
            $isFormValid = $form->isValid(); 
            
            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                $this->admin->create($business);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($business)
                    ));
                }

                $this->get('session')->setFlash('sonata_flash_success','flash_create_success');
                // redirect to edit mode
                return $this->redirectTo($business);
            }
            
            // show an error message if the form failed validation
            if (!$isFormValid) {
                $this->get('session')->setFlash('sonata_flash_error', 'flash_create_error');
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form'   => $view,
            'object' => $business,
        ));
    }
    
    public function purlsAction()
    {
        $offers = $this->getDoctrine()->getRepository(
            'SpodigEbrochureBundle:Offer'
        )->findAll();

        $segmentOffers = [];
        foreach ($offers as $offer) {
            if (!isset($segmentOffers[$offer->getView()])) {
                $segmentOffers[$offer->getView()] = [];
            }
            $segmentOffers[$offer->getView()][] = $offer;
        }
        return $this->render('SpodigEbrochureBundle:Admin:purls.html.twig', array(
            'offers' => $segmentOffers,
        ));
    }
}
