<?php

namespace Spodig\EbrochureBundle\TwigExtension;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EmailExtension extends \Twig_Extension
{   
    protected $mailer;
 
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    
    public function getFunctions()
    {
        return array(
            'sendEmail' => new \Twig_Function_Method($this, 'sendEmailAction', array('is_safe' => array('html')))
        );
    }
    
    public function getName()
    {
        return 'email_extension';
    }
    
    //TODO: Make this a service as Controller\EmailController and add additional handlers.
    public function sendEmailAction($notification, $purl, $repEmail, $customerEmail, $customerName)
    {
        $timestamp = date("h:i A M d Y");
        
        if ($notification == 'viewed') {
            $message = \Swift_Message::newInstance()
                ->setSubject('Your eBrochure Has Been Viewed - Sportsdigita Notification')
                ->setFrom('info@sportsdigita.com')
                ->setTo($repEmail)
                ->setContentType('text/html')
                ->setBody(<<<EOD
Your custom created eBrochure of <b>$purl</b> was successfully viewed by <b>$customerName</b> ($customerEmail) at <b>$timestamp</b><br>

EOD
            );
        }
        
        if ($notification == 'invoice') {
            $message = \Swift_Message::newInstance()
                ->setSubject('You Have Received an Invoice Request - Sportsdigita Notification')
                ->setFrom('info@sportsdigita.com')
                ->setTo($repEmail)
                ->setContentType('text/html')
                ->setBody(<<<EOD
You have received an invoice request through an eBrochure <b>$purl</b> from <b>$customerName</b> ($customerEmail) at <b>$timestamp</b><br>

EOD
            );
        }
                
        $this->mailer->send($message);
    }
}
