<?php

namespace Spodig\EbrochureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Spodig\EbrochureBundle\Entity\Contact as Contact;
use Spodig\EbrochureBundle\Entity\Tag as Tag;

use Highrise\HighriseAPI;

class ContactImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ebrochure:contact-import')
            ->setDescription('Import Highrise contact records from XML')
            ->addArgument('tag', InputArgument::REQUIRED, 'Name of tag to import.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $highrise = new HighriseAPI();
        $highrise->setAccount('dmenssen@sportsdigita.com');
        $highrise->setToken('129225db999f926c106fae0b62375ed3');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $contactRepo = $em->getRepository('SpodigEbrochureBundle:Contact');
        $tagRepo = $em->getRepository('SpodigEbrochureBundle:Tag');

        $people = $highrise->findPeopleByTagName($input->getArgument('tag'));
        foreach ($people as $person) {
            $emails = $person->getEmailAddresses();
            if (count($emails) < 1) {
                continue;
            }

            $existing = $contactRepo->findOneByHrId($person->getId());
            if ($existing) {
                $contact = $existing;
            } else {
                $contact = new Contact();
                $em->persist($contact);
            }

            $tags = array_map(function($tag) use ($tagRepo, $em) {
                $tagEntity = $tagRepo->findOneByHrId(trim($tag->getId()));
                if ($tagEntity) {

                    return $tagEntity;
                }

                $tagEntity = new Tag();
                $tagEntity->setHrId(trim($tag->getId()));
                $tagEntity->setName(trim($tag->getName()));
                $em->persist($tagEntity);

                return $tagEntity;
            }, $person->tags);

            $contact->setTags($tags);

            $contact->setHrId($person->getId());
            $contact->setFirstName($person->getFirstName());
            $contact->setLastName($person->getLastName());
            $contact->setEmail($emails[0]->getAddress());
            $contact->setCompanyId($person->getCompanyId());
            $contact->setCompany($person->getCompanyName());
            $contact->setTitle($person->getTitle());
            $contact->setPurl(preg_replace("/[^a-z_]/", '', strtolower($person->getFirstName() . '_' . $person->getLastName())));

            $em->flush();
        }
    }
}
