<?php

namespace Spodig\EbrochureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Spodig\EbrochureBundle\Entity\Customer as Customer;
use Spodig\EbrochureBundle\Entity\Plan as Plan;

class CustomerImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {

        $this
            ->setName('ebrochure:customer-import')
            ->setDescription('Import customer records from CSV')
            ->addArgument('source', InputArgument::REQUIRED, 'Relative path CSV file')
            ->addOption('overwrite', false, InputOption::VALUE_NONE, 'Use if you want to overwrite customer records with matching credentials')
            ->addOption('jazz', false, InputOption::VALUE_NONE, 'Special use for importing Jazz users')
            ->addOption('stars', false, InputOption::VALUE_NONE, 'Special use for importing Stars users')
            ->addOption('wizards', false, InputOption::VALUE_NONE, 'Special use for importing 13-14 Wizards users')
            ->addOption('wolves', false, InputOption::VALUE_NONE, 'Special use for importing Timberwolves users')
            ->addOption('cc', false, InputOption::VALUE_NONE, 'Special use for importing Stars additional info')
            ->addOption('fullcsv', false, InputOption::VALUE_NONE, 'Special use for importing to the Full CSV column')
            ->addOption('minimal', false, InputOption::VALUE_NONE, 'Use if you want to import customer records with ONLY Id, Last Name, and PURL')
            ->addOption('wild', false, InputOption::VALUE_NONE, 'Special use for importing Wild users')
            ->addOption('crew', false, InputOption::VALUE_NONE, 'Special use for importing Wild users')
        ;
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $userRepo = $em->getRepository('SpodigEbrochureBundle:User');
        $customerRepo = $em->getRepository('SpodigEbrochureBundle:Customer');

        $source = getcwd() . '/' . $input->getArgument('source');
        $overwrite = $input->getOption('overwrite');
        $minimal = $input->getOption('minimal');
        $wolves = $input->getOption('wolves');
        $jazz = $input->getOption('jazz');
        $stars = $input->getOption('stars');
        $cc = $input->getOption('cc');
        $fullcsv = $input->getOption('fullcsv');
        $wizards = $input->getOption('wizards');
        $wild = $input->getOption('wild');
        $crew = $input->getOption('crew');

        $crew = true;

        $row = 1;

        /*if ($wolves == true) {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {

                        $customer = new Customer();
                        $customer->setAccountId($data[0]);
                        $customer->setFirstName($data[1]);
                        $customer->setLastName($data[2]);
                        $customer->setSlug($data[7]);
                        $customer->setSegment($data[8]);

                        $salesRep = $userRepo->findOneById($data[4]);
                        $customer->setSalesRep($salesRep);

                        $em->persist($customer);
                    }
                    $row++;
                }
                fclose($handle);
                $em->flush();
            }
        }
        elseif ($crew == true) {*/
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $customer = $customerRepo->findOneBySlug($data[0]);

                        if (empty($customer))
                        {
                            $customer = new Customer();
                            $nameParts = explode(" ", $data[1]);
                            $customer->setAccountId($data[0]);
                            $customer->setFirstName($nameParts[0]);
                            if(isset($nameParts[1])){
                                $customer->setLastName($nameParts[1]);
                            } else {
                                $customer->setLastName('');
                            }

                            $customer->setSlug($data[0]);

                            $customer->setAddress1($data[3]);
                            $customer->setAddress2($data[4]);

                            if(isset($data[5])){
                                $customer->setEmail($data[5]);
                            } else {
                                $customer->setEmail('');
                            }

                            if(isset($data[2])){
                                $customer->setOrganization($data[2]);
                            }

                            $customer->setSegment('renew');
                            $customer->setPc($data[10]);
                            $salesRep = $userRepo->findOneByEmail($data[14]);
                            $customer->setSalesRep($salesRep);

                            $em->persist($customer);
                            $em->flush();
                        }

                        $plan = new Plan();

                        $plan->setPlanCode($data[6]);
                        $plan->setSection($data[7]);
                        $plan->setRow($data[8]);
                        $plan->setSeats($data[9]);
                        $plan->setTotal($data[11]);
                        $plan->setCustomer($customer);

                        $em->persist($plan);
                        $em->flush();
                    }

                    $row++;
                }
                fclose($handle);

                $em->flush();
            }
        /*}
        elseif ($wild == true) {
            $accounts = [];
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $existing = $customerRepo->findOneBySlug($data[9]);

                        if ($existing) {
                            echo "Duplicate found for Account ID: " . $data[9] . "\n";
                            continue;
                        }

                        if (isset($accounts[$data[9]])) {
                            $customer = $accounts[$data[9]];

                            if ($data[0] == '13STHFEE') {
                                $customer->setPc($customer->getPc() + $data[8]);
                            } else {
                                $customer->setMembershipLevel($customer->getMembershipLevel() . '<br>' . $data[0]);
                                $customer->setSection($customer->getSection() . '<br>' . $data[1]);
                                $customer->setRow($customer->getRow() . '<br>' . $data[2]);
                                $customer->setSeats($customer->getSeats() . '<br>' . $data[3] . ' - ' . $data[4]);
                                $customer->setNumSeats($customer->getNumSeats() . '<br>' . $data[5]);
                                $customer->setPricePerSeat($customer->getPricePerSeat() . '<br>$' . $data[6]);
                                $customer->setTotal($customer->getTotal() + $data[7]);
                                $customer->setPc($customer->getPc() + $data[8]);
                            }
                        } else {
                            $customer = new Customer();

                            $accounts[$data[9]] = $customer;

                            $customer->setSegment($data[24]);
                            $customer->setSlug($data[9]);
                            $customer->setAccountId($data[9]);
                            $customer->setEmail($data[20]);
                            $customer->setFirstName($data[11]);
                            $customer->setLastName($data[12]);
                            $customer->setAddress1($data[14]);
                            $customer->setAddress2($data[15]);
                            $customer->setCity($data[16]);
                            $customer->setPhone($data[17] . '|' . $data[18] . '|' . $data[19]);
                            $customer->setOrganization($data[13]);

                            $customer->setMembershipLevel($data[0]);
                            $customer->setSection($data[1]);
                            $customer->setRow($data[2]);
                            $customer->setSeats($data[3] . ' - ' . $data[4]);
                            $customer->setNumSeats($data[5]);
                            $customer->setPricePerSeat($data[6]);
                            $customer->setTotal($data[7]);
                            $customer->setPc($data[8]);

                            $rep = $userRepo->findOneByEmail($data[22]);
                            $customer->setSalesRep($rep);
                        }

                        $em->persist($customer);
                    }
                    $row++;

                    if ($row % 250 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($wizards == true) {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $existing = $customerRepo->findOneByEmail($data[1]);

                        if ($existing) {
                            continue;
                        }
                        $output->writeln('Create customer ' . $data[1]);

                        $customer = new Customer();

                        $customer->setSlug($data[0]);
                        $customer->setEmail($data[1]);
                        $customer->setLastName('N/A');
                        $customer->setSegment('1314');

                        $em->persist($customer);
                    }
                    $row++;

                    if ($row % 250 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($cc == true) {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $customer = $customerRepo->findOneBy([
                            'account_id' => $data[0],
                        ]);

                        if(strlen($data[2]) < 4){
                            $temp = str_pad($data[2], 4, "0", STR_PAD_LEFT);
                        } else {
                            $temp = $data[2];
                        }

                        $customer->setCustomTitle($data[1]);
                        $customer->setCustomContent($temp);

                        $em->persist($customer);
                    }
                    $row++;
                    if ($row % 20 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($fullcsv == true) {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $customer = $customerRepo->findOneBy([
                            'account_id' => $data[0],
                        ]);

                        $temp = json_decode($customer->getFullCsv(), true);
                        $temp[] = $data;
                        $customer->setFullCsv(json_encode($temp, JSON_PRETTY_PRINT));

                        $em->persist($customer);
                    }
                    $row++;
                    if ($row % 20 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($stars = true) {
            $slugs = [];
            $accounts = [];
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {

                        $data[0] = trim($data[0]);
                        if (isset($accounts[$data[0]])) {
                            $customer = $accounts[$data[0]];
                            $customer->setStartYear($customer->getStartYear().'~'.$data[52].'|'.$data[54].'|'.$data[55].'|'.$data[56].'|'.$data[57]);
                            $customer->setMembershipLevel($customer->getMembershipLevel().'~'.$data[58].'|'.$data[60].'|'.$data[61].'|'.$data[62].'|'.$data[63]);

                            if (strlen($data[67]) > 1) {
                                $tempSection = $customer->getSection();
                                if (strlen($tempSection) > 1) {
                                    $customer->setSection($tempSection+$data[67]);
                                } else {
                                    $customer->setSection($data[67]);
                                }
                            }
                            if (strlen($data[64]) > 1) {
                                $tempRow = $customer->getRow();
                                if (strlen($tempRow) > 1) {
                                    $customer->setRow($tempRow+$data[64]);
                                } else {
                                    $customer->setRow($data[64]);
                                }
                            }
                            if (strlen($data[65]) > 1) {
                                $tempSeats = $customer->getSeats();
                                if (strlen($tempSeats) > 1) {
                                    $customer->setSeats($tempSeats+$data[65]);
                                } else {
                                    $customer->setRow($data[65]);
                                }
                            }
                            if (strlen($data[68]) > 1) {
                                $tempNumSeats = $customer->getNumSeats();
                                if (strlen($tempNumSeats) > 1) {
                                    $customer->setNumSeats($tempNumSeats+$data[68]);
                                } else {
                                    $customer->setNumSeats($data[68]);
                                }
                            }

                            $customer->setPricePerSeat($customer->getPricePerSeat().'~'.$data[14].'|'.$data[16].'|'.$data[17].'|'.$data[18].'|'.$data[20].'|'.$data[19].'|'.$data[21]);
                            $customer->setPlanCode($customer->getPlanCode().'~'.$data[22].'|'.$data[24].'|'.$data[25].'|'.$data[26].'|'.$data[28].'|'.$data[27].'|'.$data[29]);
                            $customer->setTotal($customer->getTotal().'~'.$data[30].'|'.$data[32].'|'.$data[33].'|'.$data[34].'|'.$data[36].'|'.$data[35].'|'.$data[37]);
                            $customer->setPc($customer->getPc().'~'.$data[38].'|'.$data[40].'|'.$data[41].'|'.$data[42].'|'.$data[44].'|'.$data[43].'|'.$data[45]);
                            $customer->setFinalTotal($customer->getFinalTotal().'~'.$data[46].'|'.$data[48].'|'.$data[49].'|'.$data[50].'|'.$data[51]);
                            $temp = json_decode($customer->getFullCsv(), true);
                            $temp[] = $data;
                            $customer->setFullCsv(json_encode($temp, JSON_PRETTY_PRINT));

                        } else {
                            $customer = new Customer();
                            $accounts[$data[0]] = $customer;
                            $num = 1;

                            $customer->setSlug($data[0]);
                            $customer->setAccountId($data[0]);
                            $customer->setFirstName($data[1]);
                            $customer->setLastName($data[2]);

                            $customer->setAddress1($data[4]);
                            $customer->setAddress2($data[5]);
                            $customer->setCity($data[6]);
                            $customer->setState($data[7]);
                            $customer->setZip($data[8]);
                            $customer->setPhone($data[9]);
                            $customer->setEmail($data[10]);

                            //$customer->setStartYear($data[52].'|'.$data[54].'|'.$data[55].'|'.$data[56].'|'.$data[57]);
                            //$customer->setMembershipLevel($data[58].'|'.$data[60].'|'.$data[61].'|'.$data[62].'|'.$data[63]);

                            $customer->setSection($data[67]);
                            $customer->setRow($data[64]);
                            $customer->setSeats($data[65]);
                            $customer->setNumSeats($data[68]);

                            $customer->setPricePerSeat($data[14].'|'.$data[16].'|'.$data[17].'|'.$data[18].'|'.$data[20].'|'.$data[19].'|'.$data[21]);
                            $customer->setPlanCode($data[22].'|'.$data[24].'|'.$data[25].'|'.$data[26].'|'.$data[28].'|'.$data[27].'|'.$data[29]);
                            $customer->setTotal($data[30].'|'.$data[32].'|'.$data[33].'|'.$data[34].'|'.$data[36].'|'.$data[35].'|'.$data[37]);
                            $customer->setPc($data[38].'|'.$data[40].'|'.$data[41].'|'.$data[42].'|'.$data[44].'|'.$data[43].'|'.$data[45]);
                            $customer->setFinalTotal($data[46].'|'.$data[48].'|'.$data[49].'|'.$data[50].'|'.$data[51]);

                            $customer->setFullCsv(json_encode([$data], JSON_PRETTY_PRINT));


                            $customer->setSegment($data[78]);
                        }


                        $em->persist($customer);
                    }
                    $row++;
                    if ($row % 20 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($jazz == true) {
            $slugs = [];
            $accounts = [];
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {

                        $data[0] = trim($data[0]);
                        if (isset($accounts[$data[0]])) {
                            $customer = $accounts[$data[0]];
                            $customer->setSection(implode('|', $customer->getSection()) . '|' . $data[11]);
                            $customer->setRow(implode('|', $customer->getRow()) . '|' . $data[12]);
                            $customer->setSeats(implode('|', $customer->getSeats()) . '|' . $data[13]);
                            $customer->setNumSeats(implode('|', $customer->getNumSeats()) . '|' . $data[14]);

                            $customer->setPricePerSeat(implode('|', $customer->getPricePerSeat()) . '|' . $data[15]);
                            $customer->setPlanCode(implode('|', $customer->getPlanCode()) . '|' . $data[16]);
                            $customer->setTotal(implode('|', $customer->getTotal()) . '|' . $data[17]);
                            $customer->setPc(implode('|', $customer->getPc()) . '|' . $data[18]);
                            $customer->setFinalTotal($customer->getFinalTotal() + $data[19]);
                        } else {
                            $customer = new Customer();
                            $accounts[$data[0]] = $customer;
                            $num = 1;
                            $newslug = $data[21];
                            $customer->setSlug($data[21]);

                            while (in_array($newslug, $slugs)) {
                                $newslug = $data[21] . $num;
                                echo($newslug);
                                $customer->setSlug($newslug);
                                $num++;
                            }

                            $slugs[$row] = $newslug;

                            $customer->setAccountId($data[0]);
                            $customer->setOrganization($data[1]);
                            $customer->setFirstName($data[2]);
                            $customer->setLastName($data[3]);

                            $customer->setAddress1($data[4]);
                            $customer->setAddress2($data[5]);
                            $customer->setCity($data[6]);
                            $customer->setState($data[7]);
                            $customer->setZip($data[8]);
                            $customer->setPhone($data[9]);
                            $customer->setEmail($data[10]);

                            $customer->setSection($data[11]);
                            $customer->setRow($data[12]);
                            $customer->setSeats($data[13]);
                            $customer->setNumSeats($data[14]);

                            $customer->setPricePerSeat($data[15]);
                            $customer->setPlanCode($data[16]);
                            $customer->setTotal($data[17]);
                            $customer->setPc($data[18]);
                            $customer->setFinalTotal($data[19]);

                            $salesRep = $userRepo->findOneById($data[20]);
                            $customer->setSalesRep($salesRep);

                            $customer->setSegment($data[22]);
                        }


                        $em->persist($customer);
                    }
                    $row++;
                    if ($row % 20 === 0) {
                        $em->flush();
                    }
                }
                $em->flush();
                fclose($handle);
            }
        }
        elseif ($minimal == true) {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $existing = $customerRepo->findOneBySlug($data[2]);

                        if ($existing) {

                            if ($overwrite) {
                                $customer = $existing;
                            } else {
                                $output->writeln('Duplicate found for ' . $data[0] . ' (' . $data[1] . ')');
                                continue;
                            }
                        } else {
                            $customer = new Customer();
                        }
                        $customer->setLastName($data[0]);
                        $customer->setSlug($data[1]);
                        $customer->setSegment($data[2]);

                        $salesRep = $userRepo->findOneByEmail($data[2]);
                        if ($salesRep) {
                            $customer->setSalesRep($salesRep);
                        } else {
                            $output->writeln('No matching rep: ' . $data[0] . ' ' . $data[2]);
                        }

                        $em->persist($customer);
                    }
                    $row++;
                }
                fclose($handle);
                $em->flush();
            }
        }
        else {
            if (($handle = fopen($source, "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    if ($row > 1) {
                        $existing = $customerRepo->findOneByEmailOrAccountId($data[10], $data[0]);

                        if ($existing) {

                            if ($overwrite) {
                                $customer = $existing;
                            } else {
                                $output->writeln('Duplicate found for ' . $data[0] . ' (' . $data[10] . ')');
                                continue;
                            }
                        } else {
                            $customer = new Customer();
                        }
                        $customer->setAccountId($data[0]);
                        $customer->setAccountName($data[1]);
                        $customer->setFirstName($data[3]);
                        $customer->setLastName($data[4]);
                        $customer->setAddress1($data[5]);
                        $customer->setAddress2($data[6]);
                        $customer->setCity($data[7]);
                        $customer->setState($data[8]);
                        $customer->setZip($data[9]);
                        $customer->setEmail($data[10]);
                        $customer->setPhone1($data[11]);
                        $customer->setPhone2($data[12]);
                        $customer->setStartYear($data[21]);
                        $customer->setSlug($this->generateSlug());

                        $salesRep = $userRepo->findOneByEmail($data[20]);
                        if ($salesRep) {
                            $customer->setSalesRep($salesRep);
                        } else {
                            $output->writeln('No matching rep: ' . $data[0] . ' ' . $data[20]);
                        }

                        $em->persist($customer);
                    }
                    $row++;
                }
                fclose($handle);
                $em->flush();
            }
        }*/
    }

    protected function generateSlug()
    {
        $characters = 'abcdefghjkmnpqrstuvwxyz23456789';
        $lastChar = strlen($characters) - 1;
        $slug = '';
        for ($i = 0; $i < 8; $i++) {
            $slug .= $characters[rand(0, $lastChar)];
        }
        return $slug;
    }
}