<?php

namespace Spodig\EbrochureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Spodig\EbrochureBundle\Entity\Contact as Contact;
use Spodig\EbrochureBundle\Entity\Email as Email;
use Spodig\EbrochureBundle\Entity\Tag as Tag;

use Highrise\HighriseAPI;

class EmailSendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ebrochure:send-email')
            ->setDescription('Send mass email')
            ->addArgument('title', InputArgument::REQUIRED, 'Name of the email title to send')
            ->addArgument('name', InputArgument::REQUIRED, 'Your full name')
            ->addArgument('from', InputArgument::REQUIRED, 'Gmail address to send from')
            ->addOption('test', false, InputOption::VALUE_NONE, 'Don\'t actually send, just list recipients')
            // ->addArgument('password', InputArgument::REQUIRED, 'Your Gmail password')
        ;
    }



    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $test = $input->getOption('test');

        $em = $this->getContainer()->get('doctrine')->getManager();
        $mailer = $this->getContainer()->get('mailer');
        $spool = $mailer->getTransport()->getSpool();
        $transport = $this->getContainer()->get('swiftmailer.transport.real');

        $emailRepo = $em->getRepository('SpodigEbrochureBundle:Email');
        $tagRepo = $em->getRepository('SpodigEbrochureBundle:Tag');
        $contactRepo = $em->getRepository('SpodigEbrochureBundle:Contact');

        $title = $input->getArgument('title');
        $email = $emailRepo->findOneByTitle($title);
        $body = $email->getBody();

        if (!$email) {
            echo "\nNo message matches that title\n\n";
            die();
        }

        $rawTags = explode(',', $email->getTags());

        $rList = [];
        $eCount = 0;

        foreach ($rawTags as $i) {
            $t = $tagRepo->findOneByName($i);

            $recipients = $t->getContacts();

            foreach ($recipients as $r) {
                $rTo = $r->getEmail();

                $existing = in_array($rTo, $rList);

                if ($existing) {
                    echo "\nAlready sent to ".$rTo."\n";
                    continue;
                }
                array_push($rList, $rTo);
                $eCount++;

                $url = $_SERVER['PHP_SELF'];

                $b = str_replace('((purl))', $r->getPurl(), $body);
                $b = str_replace('((name))', $r->getFirstName(), $b);
                $b = str_replace('((title))', $r->getTitle(), $b);
                $b = str_replace('((company))', $r->getCompany(), $b);

                $message = \Swift_Message::newInstance()
                    ->setContentType('text/html')
                    ->setTo($rTo)
                    ->setFrom([$input->getArgument('from') => $input->getArgument('name')])
                    ->setSubject($email->getSubject())
                    ->setBody($b)
                ;

                if ($test) {
                    echo "Testing send mail to ".$r->getEmail()."\n";
                } else {
                    $mailer->send($message);
                    echo "Sending mail to ".$r->getEmail()."\n";

                    if ($eCount % 100 === 0) {
                        $spool->flushQueue($transport);
                        echo "\nFlushing mail queue and pausing for 30 seconds.\n\n";
                        sleep(30);
                    }
                }
            }
        }

        if ($test) {
            echo "\nTest sending complete.\n\n";
        } else {
            echo "\nFlushing mail queue.\n";
            $spool->flushQueue($transport);
            echo "\nMail sending successful.\n\n";
        }
    }
}
