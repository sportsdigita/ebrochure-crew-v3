<?php

namespace Spodig\EbrochureBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Spodig\EbrochureBundle\Entity\User as User;

ini_set("auto_detect_line_endings", true);

class SalesRepImportCommand extends ContainerAwareCommand
{

    protected $columns;

    protected function configure()
    {

        $this
            ->setName('ebrochure:sales-rep-import')
            ->setDescription('Import sales reps from CSV')
            ->addArgument('source', InputArgument::REQUIRED, 'Relative path CSV file')
            ->addOption('overwrite', false, InputOption::VALUE_OPTIONAL, 'Use if you want to overwrite customer records with matching credentials')
            ->addOption('easyPasswords', false, InputOption::VALUE_OPTIONAL, 'Use to set all passwords to \'Password\'')
        ;

        $this->columns = array(
            'name'     => 0,
            'phone'    => 1,
            'email'    => 2,
            'username' => 3,
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $userRepo = $em->getRepository('SpodigEbrochureBundle:User');

        $source = getcwd() . '/' . $input->getArgument('source');

        $easy = $input->getOption('easyPasswords');

        $row = 1;
        if (($handle = fopen($source, "r")) !== FALSE)
        {
            while (($data = fgetcsv($handle, 200, ",")) !== FALSE)
            {
                if ($row > 1)
                {
                    $username = $data[3];
                    $rep = $userRepo->findOneByUsername($username);

                    if (!$rep)
                    {
                        $rep = new User();
                    }
                    $encoder = $this->getContainer()
                        ->get('security.encoder_factory')
                        ->getEncoder($rep);

                    $rep->setName($data[0]);
                    $rep->setPhoneNumber($data[1]);
                    $rep->setEmail($data[2]);

                    $rep->setUsername($username);
                    $output->writeln('Username: ' . $username);
                    $rep->setSlug($username);

                    $password = $this->generatePassword($easy);
                    $output->writeln('Password: ' . $password);
                    $rep->setPlainPassword($password);
                    $rep->setPassword($encoder->encodePassword($password, $rep->getSalt()));

                    $rep->addRole('ROLE_SALES');
                    $rep->setEnabled(true);
                    $em->persist($rep);
                }
                $row++;
            }
            fclose($handle);
            $em->flush();
        }
    }

    protected function generatePassword($easy = false)
    {
        $password = 'Password';
        if (!$easy) {
            $password = array();

            $vowels = 'aeuy' . "AEUY";
            $consonants = 'bcdfghjkmnpqrstvwxz' . 'BCDFGHJKLMNPQRSTVWXZ';
            $characters = '23456789' . '@#$%';
            $numVowels = strlen($vowels);
            $numConsonants = strlen($consonants);
            $numCharacters = strlen($characters);

            for ($i = 0; $i < 3; $i++) {
                if ($i % 2 == 0) {
                    $piece = $consonants[rand(0, $numConsonants - 1)];
                    $piece .= $vowels[rand(0, $numVowels - 1)];
                    $piece .= $consonants[rand(0, $numConsonants - 1)];
                } else {
                    $piece = $characters[rand(0, $numCharacters - 1)];
                    $piece .= $characters[rand(0, $numCharacters - 1)];
                }
                $password[] = $piece;
            }
            $password = implode('-', $password);
        }
        return $password;
    }
}