<?php
namespace Spodig\EbrochureBundle\Service;

class TwigGlobalsEvent
{

    protected $twig;
    protected $config;

    public function __construct($twig, $config)
    {
        $this->twig = $twig;
        $this->config = $config;
    }

    public function onKernelRequest()
    {
        $this->twig->addGlobal('ebconfig', $this->config);
    }
}
