<?php
namespace Spodig\EbrochureBundle\Service;

class Config
{
    public static $config = [];
    private $features = [];
    private $customerTypes = [];
    private $segments = [];
    private $parameters = [];
    
    public function __construct($config)
    {
        if (!isset($config['segments']) || !is_array($config['segments'])) {
            $config['segments'] = [
                'consumer'   => 'Customer',
                'business'   => 'Business',
                'season'     => 'Customer',
            ];
        }

        $customerTypes = [];   
            
        foreach ($config['segments'] as $segment => $customerType) {
            $this->addSegment($segment, $customerType);
            foreach ((array) $customerType as $type) {
                if (!isset($customerTypes[$type])) {
                    $customerTypes[$type] = [ 'segments' => [$segment] ];
                    continue;
                }
                $customerTypes[$type]['segments'][] = $segment;
            }
        }

		if (is_array($customerTypes)) {
       		foreach ($customerTypes as $type => $data) {
          	  $this->setCustomerType($type, $data);
     	    }
     	}

        if (isset($config['features'])) {
            foreach ($config['features'] as $key => $value) {
                if (!$value) 
                {
                    continue;
                }
                $this->addFeature($key);
            }
        }

        if (isset($config['parameters'])) {
            foreach ($config['parameters'] as $key => $value) {
                $this->setParameter($key, $value);
            }
        }
    }

    public function setParameter($parameter, $value)
    {
        $this->parameters[$parameter] = $value;
    }

    public function getParameter($parameter)
    {
        if (!isset($this->parameters[$parameter])) {
            return false;
        }
        return $this->parameters[$parameter];
    }

    public function setTwig($twig)
    {
        $twig->addGlobal('ebconfig', $this);
    }

    public function setCustomerType($type, $data)
    {
        $this->customerTypes[$type] = $data;
        return $this;
    }

    public function getCustomerType($type)
    {
        if (!isset($this->customerTypes[$type])) 
        {
            return null;
        }
        return $this->customerTypes[$type];
    }
    
    public function addSegment($segment, $entity)
    {
        $this->segments[$segment] = $entity;
    }

    public function hasSegment($segment)
    {
        return isset($this->segments[$segment]);
    }

    public function getSegments()
    {
        return $this->segments;
    }

    public function getSegmentNames()
    {
        return array_keys($this->getSegments());
    }

    public function addFeature($feature)
    {
        if (in_array($feature, $this->features)) 
        {
            return $this;
        }
        $this->features[] = $feature;
        return $this;
    }

    public function hasFeature($feature)
    {
        return in_array($feature, $this->features);
    }
}
