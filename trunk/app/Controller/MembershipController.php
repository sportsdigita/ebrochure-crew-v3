<?php
namespace Controller;

use Symfony\Component\HttpFoundation\Response;
use Spodig\EbrochureBundle\Entity\Renewal;

class MembershipController
{
    private $doctrine;
    private $templating;
    private $request;
    private $query;
    private $router;

    protected $mailer;

    public function __construct($doctrine, $templating, $request, $router, \Swift_Mailer $mailer)
    {
        $this->doctrine = $doctrine;
        $this->templating = $templating;
        $this->request = $request;
        $this->router = $router;
        $this->mailer = $mailer;
    }

    public function invoiceAction($customerSlug)
    {
        $em = $this->doctrine->getEntityManager();

        $customer = $em
            ->getRepository('SpodigEbrochureBundle:Customer')
            ->findOneBySlug($customerSlug);

        $rep = $customer->getSalesRep();

        $content = $this->templating->render(
            '::invoice.html.twig', [
                'customer' => $customer,
                'rep' => $rep,
            ]
        );

        $r = new Response();
        $r->setContent($content);
        return $r;

    }

    public function pdfAction($accountId)
    {

        $path = __DIR__ . '/../../pdf/' . $accountId . '.pdf';
        if (file_exists($path)) {
            unlink($path);
        }
        putenv('PATH=' . getenv('PATH') . ':/usr/bin:/bin:/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/bin/wkhtmltopdf.app/Contents/MacOS');
        if (file_exists('/usr/local/bin/wkhtmltopdf')) {
            $binary = '/usr/local/bin/wkhtmltopdf';
        } else {
            $binary = '/usr/local/bin/wkhtmltopdf.app/Contents/MacOS/wkhtmltopdf';
        }
        // if ($this->get('kernel')->getEnvironment() != 'dev') {
           $binary = "/usr/bin/xvfb-run -a -s \"-screen 0 640x480x16\" $binary --use-xserver";
        // }
        $bodyUrl = $this->router->generate('membership_pdfinvoice', ['customerSlug' => $accountId], true);
        $orientation = 'Portrait';

        $command = escapeshellcmd("$binary --background --orientation $orientation --disable-smart-shrinking -s letter -B 0.5in -L 0.5in -R 0.5in -T 0.5in $bodyUrl $path");

        exec($command . ' 2>&1', $output);

        $r = new Response();
        $r->setContent(file_get_contents($path));
        $r->setStatusCode(200);
        $r->headers->set('Content-Disposition', 'attachment;filename="' . $accountId . '.pdf"');
        $r->headers->set('Content-Type', 'application/pdf');
        return $r->send();
    }

    public function testAction()
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Test Email')
            ->setFrom('no-reply@sportsdigita.com')
            ->setTo('mwillbanks@sportsdigita.com')
            ->setBody('Test email');
        $this->mailer->send($message);
        die('success');
    }
}

