<?php
namespace Controller;

use Symfony\Component\HttpFoundation\Response;

class UniformController
{
    public function __construct($request)
    {
        $this->request = $request;
    }

    public function generateAction()
    {
        $r = new Response();
        $nameText = $this->request->query->get('name');
        $numberText = $this->request->query->get('number');

        $numberText = preg_replace("/[^0-9]/", '', $numberText);
        // $nameText = trim(preg_replace("/[^A-Z]/", '' , strtoupper($nameText)));
        $nameText = strtoupper($nameText);
        $nameLength = strlen($nameText);

        if (!$nameText || $numberText == NULL) 
        {
            $r->setContent('error');
            return $r;
        }

        $filename = 'My_Columbus_Crew_Jersey_' . md5($numberText . $nameText) . '.jpg';
        $filepath = __DIR__ . '/../../web/uniforms/' . $filename;
        $r->setContent($filename);

        $name = new \ImagickDraw();
        $name->setTextAlignment(2); // (2 == CENTER)
        $name->setStrokeWidth(0);
        //$name->setGravity( \Imagick::GRAVITY_CENTER );
        $name->setFont(__DIR__ . '/../../web/fonts/jersey-text.ttf');
        $name->setFillColor('#000');
        $name->setTextKerning(5);

        $number = new \ImagickDraw();
        $number->setTextAlignment(2); // (2 == CENTER)
        $number->setStrokeWidth(0);
        $number->setFont(__DIR__ . '/../../web/fonts/jersey-text.ttf');
        $number->setFillColor('#000');
        $number->setFontSize('90');
        $number->setTextKerning(1);

        $upperNumber = new \ImagickDraw();
        $upperNumber->setTextAlignment(2); // (2 == CENTER)
        $upperNumber->setStrokeWidth(1);
        $upperNumber->setStrokeColor('#838b81');
        $upperNumber->setFont(__DIR__ . '/../../web/fonts/jersey-text.ttf');
        $upperNumber->setFillColor('#FFF');
        $upperNumber->setFontSize('50');
        $upperNumber->setTextKerning(1);

        $upperName = new \ImagickDraw();
        $upperName->setTextAlignment(2); // (2 == CENTER)
        $upperName->setStrokeWidth(0);
        //$upperName->setStrokeColor('#86660f');
        //$upperName->setStrokeAntialias(true);
        $upperName->setFont(__DIR__ . '/../../web/fonts/jersey-text.ttf');
        $upperName->setFillColor('#FFF');
        $upperName->setFontSize('200');
        $upperName->setTextKerning(1);

        $nameImg = new \Imagick();
        $nameImg->newImage(200, 315, new \ImagickPixel('transparent'));

        $numberImg = new \Imagick();
        $numberImg->newImage(220, 240, new \ImagickPixel('transparent'));

        $upperNameImg = new \Imagick();
        $upperNameImg->newImage(400, 400, new \ImagickPixel('transparent'));

        $upperNumberImg = new \Imagick();
        $upperNumberImg->newImage(400, 400, new \ImagickPixel('transparent'));

        $textProperties = array('textWidth' => 0);
        $fontSize = 0;

        while ($textProperties['textWidth'] <= 200) 
        {
            ++$fontSize;

            $name->setFontSize($fontSize);
            $textProperties = $nameImg->queryFontMetrics($name, $nameText);

            if ($textProperties['textHeight'] > 50) 
            {
                break;
            }
        }

        $name->setFontSize($fontSize - 1);
        $upperName->setFontSize($fontSize -1);

        $arcArray = array(35);
        $nameImg->annotateImage($name, 100, 170, 3, $nameText); // add the name to the back of the jersey
        $nameImg->setImageBackgroundColor('transparent');
        $nameImg->distortImage( \Imagick::DISTORTION_ARC, $arcArray, false);
        $nameImg->waveImage(-8, 1000);
        $nameImg->resizeImage(165, 225, \Imagick::FILTER_LANCZOS, 1);

        $numberImg->annotateImage($number, 120, 100, 1, $numberText); // add the number to the back of the jersey
        $numberImg->resizeImage(300, 380, \Imagick::FILTER_LANCZOS, 1);

        if (strlen($nameText) > 7)
        {
            $upperNameImg->resizeImage(120, 160, \Imagick::FILTER_LANCZOS, 1);
        } 
        else 
        {
            $upperNameImg->resizeImage(120, 160, \Imagick::FILTER_LANCZOS, 1);
        }

        //$upperNumberImg->annotateImage($upperNumber, 110, 50, -1, $numberText); // add the number to the helmet
        //$upperNumberImg->resizeImage(100, 100, \Imagick::FILTER_LANCZOS, 1);

        $img = new \Imagick(__DIR__ . '/../../web/uniforms/base.jpg');

        $img->compositeImage($nameImg, \Imagick::COMPOSITE_DEFAULT, 390, 45);
        $img->compositeImage($numberImg, \Imagick::COMPOSITE_DEFAULT, 305, 130);

        // if (strlen($nameText) > 7)
        // {
        //     $img->compositeImage($upperNameImg, \Imagick::COMPOSITE_DEFAULT, 385, 1);
        // } 
        // else 
        // {
        //     $img->compositeImage($upperNameImg, \Imagick::COMPOSITE_DEFAULT, 385, 1);
        // }

        //$img->compositeImage($upperNumberImg, \Imagick::COMPOSITE_DEFAULT, 414, 18);

        $img->writeImage($filepath);

        return $r;
    }

    private function getImage($filename)
    {
        $filename = __DIR__ . "/../../web/uniforms/$filename";
        $response = new Response();
        if (!file_exists($filename)) {
            $r->setContent('File does not exist.');
            return $r;
        }

        $response->headers->set('Content-Type', 'image/png');
        $response->setContent(file_get_contents($filename));
        return $response;
    }

    public function viewAction($filename)
    {
        return $this->getImage($filename);
    }

    public function downloadAction($filename)
    {
        $response = $this->getImage($filename);
        $response->headers->set('Content-Disposition', 'attachment');
        return $response;
    }
}
