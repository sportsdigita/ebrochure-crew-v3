# Class: phpmyadmin
#
# This installs phpMyAdmin on your machine.
# Needed passwords have to be provided by custom facts. In this way
# no password has to be added to resource calls.
# Look at the custom facter provided by this module
# to get to know how the needed facts have to be stored on the machine.
#
# Requires:
#   non-empty password for root
#   fact: pma_mysql_root_password
#   fact: pma_controluser_password
#
# Example Usage:
#
#   class { 'phpmyadmin':
#   }
#
# @TODO configuration,configuration,configuration ...
#
class phpmyadmin (
) {

  exec {
      'phpmyadmin':
          command => "wget http://downloads.sourceforge.net/project/phpmyadmin/phpMyAdmin/3.5.8.2/phpMyAdmin-3.5.8.2-english.tar.gz -O phpmyadmin.tar.gz && tar -zxvf phpmyadmin.tar.gz && mv phpMyAdmin* phpmyadmin && touch $puppet_lock_dir/phpmyadmin-download",
          creates => "$puppet_lock_dir/phpmyadmin-download",
          cwd => '/usr/share',
          user => 'root',
          require => File[$puppet_lock_dir];
  }
  ->
  file { '/usr/share/phpmyadmin/config.inc.php':
    ensure => link,
    owner  => root,
    group  => root,
    mode   => '0644',
    target => '/etc/phpmyadmin/config.inc.php',
  }
  ->
  # phpMyAdmin will display a warning if its control user 
  # is equal to the mysql root user and so
  # we create the default phpmyadmin user here.
  # On some machines the phpmyadmin user might be already installed.
  # We have to drop and re-add this user because of new privileges and password.
  exec{ 'creating-phpmyadmin-controluser':
    command => "echo \"CREATE USER 'phpmyadmin'@'localhost'\
      IDENTIFIED BY '${::pma_controluser_password}';\
      GRANT ALL ON *.* TO 'phpmyadmin'@'localhost';FLUSH PRIVILEGES;\"\
      | mysql -u root -p'${::pma_mysql_root_password}'",
    path    => ['/usr/local/bin', '/usr/bin', '/bin'],
    unless  => "mysql -u root -p'${::pma_mysql_root_password}'\
      -e 'select * from mysql.user WHERE User=\"phpmyadmin\"' \
      | grep 'phpmyadmin'"
  }
}
