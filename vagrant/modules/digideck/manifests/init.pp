class digideck {

    $mysqlRootPasswordLockFile = "$puppet_lock_dir/mysql_root_password_set"
    $mysqlDataLockFile = "$puppet_lock_dir/mysql_setup_data_database"

    exec {
        'mysql_root_password':
            command => "mysqladmin -u root password root && touch $mysqlRootPasswordLockFile",
            creates => "$mysqlRootPasswordLockFile",
            require => [Service['mysqld'], File[$puppet_lock_dir] ];

        'mysql_setup_data_database':
            command => "mysql -u root -proot mysql -e 'DROP DATABASE IF EXISTS digideck; CREATE DATABASE IF NOT EXISTS digideck;' && mysql -u root -proot digideck < /var/www/current-data-dump.sql && touch $mysqlDataLockFile",
            creates => "$mysqlDataLockFile",
            require => [
                Exec['mysql_root_password'],
                Service['mysql'],
                File[$puppet_lock_dir]
            ];
    }
}