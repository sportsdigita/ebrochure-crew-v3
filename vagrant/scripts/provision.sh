#!/bin/sh

# Setup all provisioning steps for puppet
if [ ! -f /apt-get-run ]; then sudo apt-get update && sudo touch /apt-get-run; fi
if [ ! -f /apt-get-puppet ]; then sudo apt-get install --yes --force-yes puppet && sudo touch /apt-get-puppet; fi


echo "mysql_root_password=root" > /etc/phpmyadmin.facts;
echo "controluser_password=awesome" >> /etc/phpmyadmin.facts;