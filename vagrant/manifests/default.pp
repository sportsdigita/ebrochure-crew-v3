group { 'puppet': ensure => present }
Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
File { owner => 0, group => 0, mode => 0644 }

$puppet_lock_dir = '/var/puppet_locks/'

Class['::apt::update'] -> Package <|
    title != 'python-software-properties'
and title != 'software-properties-common'
|>

apt::key { '4F4EA0AAE5267A6C': }

apt::ppa {
    'ppa:ondrej/php5-oldstable':
        require => Apt::Key['4F4EA0AAE5267A6C'];
}

package {
    [
      'build-essential',
      'vim',
      'curl',
      'git-core',
      'htop'
    ]:
        ensure  => 'installed';

#    'apache2':
#        ensure => purged,
#        provider => 'apt',
#        notify => Service['nginx'],
#        require => Exec['apache2'];

}

#exec{
#    'apache2':
#        command => "killall apache2 -q || touch $puppet_lock_dir/killed-apache",
#        creates => "$puppet_lock_dir/killed-apache",
#        user => 'root',
#        require => [File[$puppet_lock_dir] ];
#}

php::module {
  [
    'php5-mysql',
    'php5-cli',
    'php5-curl',
    'php5-intl',
    'php5-mcrypt',
    'php5-gd',
    'php5-imagick'
  ]:
  service => 'php5-fpm',
}

service { 'php5-fpm':
  ensure     => running,
  enable     => true,
  hasrestart => true,
  hasstatus  => true,
  require    => Package['php5-fpm'],
}

class {
    'apt':
        always_apt_update => true;
    'nginx':;
    'php':
        package             => 'php5-fpm',
        service             => 'php5-fpm',
        service_autorestart => false,
        config_file         => '/etc/php5/fpm/php.ini',
        module_prefix       => '';
    'xdebug':
        service => 'nginx';
    'php::devel':
        require => Class['php'];
    'composer':
        require => Package['php5-fpm', 'curl'];
    'mysql::server':
        config_hash   => { 'root_password' => 'root' };
    'phpmyadmin':
        require => [Service['mysqld'], Class['php']];
}

puphpet::ini {
    'symfony':
        value   => [
            'display_errors = On',
            'error_reporting = -1',
            'xdebug.max_nesting_level = 250',
            'short_open_tag = 0'
        ],
        ini     => '/etc/php5/conf.d/zzz_symfony.ini',
        notify  => Service['php5-fpm'],
        require => Class['php'];
    'php':
        value   => [
            'date.timezone = "America/Chicago"',
            'apc.enabled = 1'
        ],
        ini     => '/etc/php5/conf.d/zzz_php.ini',
        notify  => Service['php5-fpm'],
        require => Class['php'];
    'xdebug':
        value   => [
            'xdebug.profiler_enable = 0',
            'xdebug.default_enable = 0',
            'xdebug.remote_autostart = 0',
            'xdebug.remote_connect_back = 1',
            'xdebug.remote_enable = 0',
            'xdebug.remote_handler = "dbgp"',
            'xdebug.remote_port = 9000'
        ],
        ini     => '/etc/php5/conf.d/zzz_xdebug.ini',
        notify  => Service['php5-fpm'],
        require => Class['php'];
    'php-fpm':
        ini => '/etc/php5/fpm/php-fpm.conf',
        value   => [
            '[global]',
            'pid = /var/run/php5-fpm.pid',
            'error_log = /var/log/php-fpm/php5-fpm.log',
            'include=/etc/php5/fpm/pool.d/*.conf'
        ],
        notify  => Service['php5-fpm'],
        require => Class['php'];
    'php-fpm-pool':
        ini => '/etc/php5/fpm/pool.d/www.conf',
        value   => [
            '[www]',
            'user = www-data',
            'group = www-data',
            'listen = /var/run/php5-fpm.sock',
            'request_slowlog_timeout = 5s',
            'slowlog = /var/log/php-fpm/slowlog-site.log',
            'listen.allowed_clients = 127.0.0.1',
            'listen.backlog = -1',
            'rlimit_core = unlimited',
            'rlimit_files = 131072',
            'request_terminate_timeout = 120s',
            'catch_workers_output = yes',
            'pm = dynamic',
            'pm.max_children = 5',
            'pm.start_servers = 3',
            'pm.min_spare_servers = 2',
            'pm.max_spare_servers = 4',
            'pm.max_requests = 200',
            'pm.status_path = /status',
            'chdir = /'
        ],
        notify  => Service['php5-fpm'],
        require => [Class['php'], File['/var/log/php-fpm/']];
}

nginx::resource::vhost {
    'primary':
        ensure       => present,
        server_name  => [
            'awesome.dev',
            'localhost',
            '127.0.0.1',
            '192.168'
        ],
        listen_port  => 80,
        www_root     => '/var/www/trunk/web/',
        index_files  => ['app.php', 'app_dev.php'],
        try_files    => ['$uri', '@rewriteapp'],
        notify       => Class['nginx::service'],
        vhost_cfg_append => {
            'rewrite' => '^/app\.php/?(.*)$ /$1 permanent',
        },
        include_files => ['/etc/nginx/nginx-security.conf'],
        require => File['nginx-security']
}


$path_translated = 'PATH_TRANSLATED $document_root$fastcgi_path_info'
$script_filename = 'SCRIPT_FILENAME $document_root$fastcgi_script_name'
$fastcgi_config  = {
    'fastcgi_split_path_info'       => '^(.+\.php)(/.*)$',
    'fastcgi_param'                 => ' PATH_INFO $fastcgi_path_info',
    'fastcgi_param '                => $path_translated,
    'fastcgi_param  '               => $script_filename,
    'fastcgi_pass'                  => 'unix:/var/run/php5-fpm.sock',
    'fastcgi_index'                 => 'index.php',
    'include'                       => 'fastcgi_params',
    'fastcgi_connect_timeout'       => '60',
    'fastcgi_send_timeout'          => '180',
    'fastcgi_read_timeout'          => '180',
    'fastcgi_buffer_size'           => '128k',
    'fastcgi_buffers'               => '4 256k',
    'fastcgi_busy_buffers_size'     => '256k',
    'fastcgi_temp_file_write_size'  => '256k',
    'fastcgi_intercept_errors'      => 'on'
}

nginx::resource::location {
    'rewriteapp':
        ensure              => 'present',
        vhost               => 'primary',
        location            => '@rewriteapp',
        www_root            => '/var/www/trunk/web/',
        index_files         => ['app.php', 'app_dev.php'],
        location_cfg_prepend => {'rewrite' => '^(.*)$ /app.php/$1 last'};

    'primary-fastcgi-config':
        ensure              => 'present',
        vhost               => 'primary',
        location            => '~ ^/(app|app_dev|config)\.php(/|$)',
        index_files         => ['app.php', 'app_dev.php'],
        proxy               => undef,
        try_files           => ['$uri', '$uri/', '/app.php?$args', '/app_dev.php?$args'],
        www_root            => '/var/www/trunk/web/',
        location_cfg_append => $fastcgi_config,
        notify              => Service['nginx'];

    'phpmyadmin':
        ensure              => 'present',
        vhost               => 'primary',
        location            => '/phpmyadmin',
        index_files         => ['index.php'],
        proxy               => undef,
        www_root            => '/usr/share/',
        extra_block         => '
            location ~ ^/phpmyadmin/(.+\.php)$ {
                try_files $uri =404;
                root /usr/share/;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                fastcgi_param PATH_INFO $fastcgi_path_info;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
                include fastcgi_params;
            }
            location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
                root /usr/share/;
            }',
        notify              => Service['nginx'];

    'dotfiles':
        ensure              => 'present',
        vhost               => 'primary',
        location            => '~ /\.',
        www_root            => '/var/www/',
        location_cfg_append => {
            'access_log'    => 'off',
            'log_not_found' => 'off',
            'deny'          => 'all'
        };
}


file {
    $puppet_lock_dir:
        ensure => 'directory',
        owner => "root",
        group => "root";
    '/etc/nginx/conf.d/default.conf':
        ensure => absent,
        notify => Service['nginx'];
    '/var/log/php-fpm/':
        ensure => 'directory',
        owner => "root",
        group => "root",
        mode => 664;
    'nginx-security':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        path => '/etc/nginx/nginx-security.conf',
        source => 'puppet:///modules/digideck/nginx-block.cfg';
}

composer::run {
  'install':
      path => '/var/www/trunk/',
      command => 'install',
      #creates => '/var/www/composer.lock',
      require => Class['composer'];
}

include dotfiles, digideck